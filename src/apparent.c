/**
 *
 * Apparent utilities module on the AMU (Apparent Management Unit).
 *
 * Copyright (c) 2021 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 *
 * This module contains utility support routines for the Apparent Gateway PCBA system.
 *
 *
 */


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "pmc.h" // ???
#include "ASF/sam/utils/cmsis/same70/include/instance/pioc.h"

#include "usart.h"
#include "status_codes.h"
#include "uart_serial.h"

#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "board.h"

#include "apparent.h"
#include "amu_common/flashinator.h"
#include "amu_common/rgbLedActivity.h"
#include "apparent_strings.h"


//
// Apparent note:
//
// The flash utility in the src/ASF/sam/flash_efc folder contains the 2 flash_efc.c and flash_efc.h files
// for the interface to the SAME70 "user signature" area of flash (Section "22.4.3.9 User Signature Area"
// in the SAME70 data sheet).
//
// Problem: after adding those 2 files to the project, the apparent.c file failed to locate the flash_efc.h
// header for inclusion in the build. The instructions to add this to the linker specified to:
//
// - open: Project -> AMU Properties -> ARM/GNU C Compiler -> Directories
// - click on "Add Files"
//
// But this simply did not work. So live with this funky relative path.

#include "ASF/sam/flash_efc/flash_efc.h"

extern U32  g_ul_ms_ticks;
extern BOOL g_b_led0_active;

// In main.c module:
extern void mdelay(uint32_t ul_dly_ticks);
extern void setButtonChangeTimestamp(void);

extern uint32_t _sfixed;

extern BOOL debugDropTheNextIncomingDataPacket;

// The SAME70 reset controller status register contains the last reset reason.
t_same70ResetInfoDataBlock same70ResetInfoDb;

// A FEW AMU/APPARENT-SPECIFIC ITEMS.


// CLI.
//
// Every good system has a set. And so do bad ones. 
//
// Note: all commands are available for execution over the USB interface.
//       The help facility could make for a dangerous user "on the other side",
//       especially from the (Linux) terminal. Its removal is under consideration.
//
// Note: any change to any string that equates to a CLI command must be accompanied by a
//       corresponding change on the SBC/Gateway code base.
//
// CLI RESPONSES: all responses are expected to be of the form:
//
// {"CLI":{"COMMAND":"the_actual_command","ARGUMENTS":"if any","CODE":0,"RESPONSE":{"somewhat free-form","RESULT":"SUCCESS"}}
//
// CLI Group 1: the set of mandatory high-level commands, used by the SBC/Gateway
//              to maintain overall system integrity and sanity of the PCBA. This
//              set of commands are considered "structured".
const char KEEP_ALIVE_HELLO_CMD[]                          = "hello";                         //  1
const char GET_AUTONOMOUS_EVENTS_CMD[]                     = "get_auto_events";               //  2
const char COUNTERS_CMD[]                                  = "counters";                      //  3
const char UPGRADE_CMD[]                                   = "upgrade";                       //  4
const char RESET_CMD[]                                     = "reset";                         //  5
const char PROGRAM_CMD[]                                   = "program";                       //  6
const char DISPLAY_PROGRAM_SIGNATURES_CMD[]                = "display_program_signatures";    //  7

// CLI Group 2: test and debug-centric commands, not intended for regular use by the SBC/Gateway.
const char DISPLAY_BOOTLOADER_BANNER_CMD[]                 = "banner";                        //  8
const char DISPLAY_HELP_CMD[]                              = "heeeeelp";                      //  9
const char ENABLE_UPGRADE_INFO_CMD[]                       = "enable_upgrade_info";           // 10
const char DISABLE_UPGRADE_INFO_CMD[]                      = "disable_upgrade_info";          // 11
const char ENABLE_UPGRADE_HEX_INFO_CMD[]                   = "enable_upgrade_hex_info";       // 12
const char DISABLE_UPGRADE_HEX_INFO_CMD[]                  = "disable_upgrade_hex_info";      // 13
const char SET_LOCAL_TESTING_CMD[]                         = "set_local_testing";             // 14
const char NOP_CMD[]                                       = "test_nop";                      // 15

const char DROP_UPGRADE_DATA_PACKET_CMD[]                  = "drop_upgrade_data_packet";      // 16
const char DROP_UPGRADE_PACKET_ACK_CMD[]                   = "drop_upgrade_packet_ack";       // 17

// These may or may not be included in the build, depending on the INCLUDE_CLI_FLASH_COMMANDS in the
// apparent.h header file. For a build to be deployed, they are not compiled into the build. Only in
// an engineering/test environment should they be included.
const char PRINT_FH_CMD[]                                  = "print_fh";                      // 18
const char SET_FH_PATTERN_CMD[]                            = "set_fh_pattern";                // 19
const char CRC_FLASH_BLOCK_CMD[]                           = "crc_flash_block";               // 20
const char IS_FLASH_RANGE_ERASED_CMD[]                     = "is_flash_range_erased";         // 21
const char READ_FLASH_LONG_CMD[]                           = "read_flash_long";               // 22
const char WRITE_FLASH_LONG_CMD[]                          = "write_flash_long";              // 23
const char READ_FLASH_BLOCK_CMD[]                          = "read_flash_block";              // 24
const char WRITE_FLASH_BLOCK_CMD[]                         = "write_flash_block";             // 25

// END ... OF CLI RELATED ITEMS.

#if 0 // RGB LED ACTIVITY
// To control the cadence of the RGB LED activity status indicator.
t_ledActivityControlBlock ledActivityCb;
#endif

// To calculate the 32-bit CRC over the user signature:
static const U32 CRC32_TABLE[] =
{
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
    0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
    0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
    0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
    0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
    0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
    0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
    0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
    0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
    0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
    0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
    0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
    0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236, 0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
    0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
    0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
    0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
    0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
    0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};


// BAD EVENTS COUNTERS:
static U32 amuBadEventsCountersBlock[MAX_BAD_EVENTS_COUNTERS];


// Data to manage user input over UART0/SBCCOM interface.
static char pSbcComInputBuffer[SBCCOM_INPUT_BUFFER_LENGTH];
U32         sbcComInputBufferRank;     // rank into which the next input character is written
BOOL        sbcComInputReady;          // <- written to directly by UART0_Handler() interrupt routine.
BOOL        inputActivelyProcessed;    // indicates that the application is actively processing the input string
BOOL        sbcComEchoChar;            // <- interrupt routine: echo each character received if TRUE
static BOOL inputChecksumRequired;     // TRUE by default except when disabled for local testing

// Data to manage user input over UART1/CRAFT interface.
static char pCraftInputBuffer[CRAFT_INPUT_BUFFER_LENGTH];
U32         craftInputBufferRank;      // rank into which the next input character is written
BOOL        craftInputReady;    // <- written to directly by UART1_Handler() interrupt routine.

// Application/boot load flash addresses:
U32  bootloaderLoadFlashAddress;
U32  applicationLoadFlashAddress;

// Application/boot flash signatures:
U32  bootloaderFlashSignature;
U32  applicationFlashSignature;

BOOL softwareResetPlease;
U32  softwareResetTimestamp;

// GLOBAL DATA:
U32  amuOutputChecksum;
U16  charsWrittenByInterrupt;
U16  totalUartCharsWritten;

// Static Module prototypes:
static void  displayControllerResetReason        (BOOL jsonYes);
static void  validateMyBootSignature             (void);
static void  helloResponse                       (void);
static void  displayApparentMiniHelp             (void);
static void  executeNOP                          (void);
static void  initApparentData                    (void);
static void  initAmuCounters                     (void);
static void  displayAndResetBadEventsCounters    (void);
static void  processUart0InputString             (char *pUserInput);
static void  processUart1InputString             (char *pUserInput);
static BOOL  isInputChecksumValid                (char *pUserInput);

#if 0 // RGB LED ACTIVITY
static void  setRgbLed                           (U32 gpio, U32 direction, U32 level);
#endif

static void  resetFunc                           (void);
static void  programResponse                     (void);
static void  bootloaderIncrementBadEventCounter  (U16  badEventIndex);
static void  getAutonomousEvents                 (void);
static void  executeSoftwareResetAction          (void);
static void  readOutProgramFlashSignatures       (void);
static void  findComponentStringData             (const char *pComponent, char *pValueString, char *pArguments);
static U32   findEndOfInputRank                  (char * pUserInput);
static void  processUpgradeCommand               (char *pUserInput);
static U32   readOneProgramFlashSignature        (U32 baseMemoryAddress);
static void  toggleChecksumRequired              (void);

#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
static void  setFlashHoldingPattern              (char *pUserInput);
static void  readFromFlashMemoryIntoHolding      (char *pUserInput);
static void  writeFromHoldingIntoFlashMemory     (char *pUserInput);
static void  calculateAndPrintHoldingBlockCrc    (void);
static void  isCheckFlashErasedTestCommand       (char *pUserInput);
static void  readFlashCommand                    (char *pUserInput);
static void  writeFlashLongCommand               (char *pUserInput);
static BOOL  isFlashErasedMemoryRange            (U32 baseMemoryAddress, U32 numberOfInstructionWords);
static void  readFlashMemoryRange                (U32 baseMemoryAddress, U32 numberOfInstructionWords);
#endif



/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond


// Exported/external functions from this module:


// getProcessorResetReason
//
// This function is identical on the bootloader program, and ought to be in a comon module.
//
// Get the SAME70 reset reason.
void getProcessorResetReason(void)
{
    same70ResetInfoDb.status       =  RSTC->RSTC_SR;
    same70ResetInfoDb.statusMasked = same70ResetInfoDb.status & RSTC_SR_RSTTYP_Msk;

    if      ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_GENERAL_RST) == RSTC_SR_RSTTYP_GENERAL_RST) { same70ResetInfoDb.reason = RESET_REASON_GENERAL;  }
    else if ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_BACKUP_RST)  == RSTC_SR_RSTTYP_BACKUP_RST)  { same70ResetInfoDb.reason = RESET_REASON_BACKUP;   }
    else if ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_WDT_RST)     == RSTC_SR_RSTTYP_WDT_RST)     { same70ResetInfoDb.reason = RESET_REASON_WATCHDOG; }
    else if ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_SOFT_RST)    == RSTC_SR_RSTTYP_SOFT_RST)    { same70ResetInfoDb.reason = RESET_REASON_SOFTWARE; }
    else if ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_USER_RST)    == RSTC_SR_RSTTYP_USER_RST)    { same70ResetInfoDb.reason = RESET_REASON_NRST_LOW; }
    else                                                                                                  { same70ResetInfoDb.reason = RESET_REASON_UNKNOWN;  }
}


// initApparentSubsystemData
//
// Called from main during system startup.
//
// Note: that the bootloader does not read out any data from the SAME70 User Signature flash zone.
//
void initApparentSubsystemData(void)
{
    U32 *pMyFlashAddress = (uint32_t *) & _sfixed;
    initApparentData();
    initCraftInputBuffer();
    initSbcComInputBuffer();
    readOutProgramFlashSignatures();
    validateMyBootSignature();

    bootloaderLoadFlashAddress = (U32)pMyFlashAddress - IFLASH_START_ADDRESS;

    initRgbLedActivityData();

}


//
//
//
//
static void initAmuCounters(void){
    U32 iggy;
    for (iggy = 0; iggy < MAX_BAD_EVENTS_COUNTERS; iggy++) {
        amuBadEventsCountersBlock[iggy] = 0;
    }
}


// displayAndResetBadEventsCounters//
//
// Display the counters from the "bad events" counter data block. Only include non-zero counts.
//
// {"CLI":{"COMMAND":"counters","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"AMU COUNTERS":[{"RANK":1,"COUNT":111},{"RANK":2,"COUNT":222}, ... ,{"RANK":10,"COUNT":333}]},"RESULT":"SUCCESS"}}
//
//
static void displayAndResetBadEventsCounters(void)
{
    U32 iggy;
    U32 firstTime = TRUE;

    // The preamble. Counters are returned in a list, so use a square bracket to open the list.
    printf("{%s:{%s:\"%s\",%s:%s,%s:0,%s:{%s:[", CLI_STRING, COMMAND_STRING, COUNTERS_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, AMU_COUNTERS_STRING);

    // Now the counters data.
    for (iggy = 0; iggy < MAX_BAD_EVENTS_COUNTERS; iggy++) {
        if (amuBadEventsCountersBlock[iggy] != 0) {
            if (firstTime) { firstTime = FALSE; }     // The 1st counter has no leading comma
            else           { printf(",");       }     // All other counters have the leading comma
            printf("{%s:%lu,%s:%lu}",  RANK_STRING, iggy, COUNT_STRING, amuBadEventsCountersBlock[iggy]);
        }                
    }
    // Terminate: - the list with ]
    //            - "RESPONSE" with }
    //            - include the result/success
    //            - close off the entire json-like string with }}
    printf("]},%s:%s}}\r\n", RESULT_STRING, SUCCESS_STRING);
    initAmuCounters(); // Reset the counters.
}


// validateMyBootSignature
//
// Validates the bootloader signature read out from flash. If a NULL signature was read out,
// the pre-defined hard-coded signature is written to flash.
static void validateMyBootSignature(void)
{
    if (bootloaderFlashSignature == NULL_SIGNATURE_VALUE) {
        writeFlashProgramSignature(BOOTLOADER_SIGNATURE_ADDRESS, BOOTLOADER_SIGNATURE_VALUE);
        readOutProgramFlashSignatures();
    }
}


/**
 * Apparent
 * Called by the UART0 interrupt handler to write a character to the user input buffer.
 * Check for buffer overflow. No printf() or other such calls: it's not polite doing
 * those kind of things from within an interrupt handler.
 */
void writeToSbcComInputBuffer(char oneChar)
{
    if (inputActivelyProcessed) { bootloaderIncrementBadEventCounter(SBC_INPUT_WHILE_PROCESSING); }

    if (sbcComInputBufferRank < SBCCOM_INPUT_BUFFER_LENGTH) {
        pSbcComInputBuffer[sbcComInputBufferRank++] = oneChar;
        if (sbcComInputBufferRank > SBCCOM_INPUT_BUFFER_SAFETY_MARGIN) { bootloaderIncrementBadEventCounter(UART0_INPUT_BUFFER_ABOVE_SAFETY_MARGIN); }   
    } else {
        // ICK! Buffer is full.
        bootloaderIncrementBadEventCounter(UART0_INPUT_BUFFER_OVERFLOW);
        initSbcComInputBuffer();
    }
}


/**
 * Apparent
 * Called by the UART1 interrupt handler to write a character to the user input buffer.
 * Check for buffer overflow. No printf() or other such calls: it's not polite doing
 * those kind of things from within an interrupt handler.
 */
void writeToCraftInputBuffer(char oneChar)
{
    if (craftInputBufferRank < CRAFT_INPUT_BUFFER_LENGTH) {
        pCraftInputBuffer[craftInputBufferRank++] = oneChar;
        if (craftInputBufferRank > CRAFT_INPUT_BUFFER_SAFETY_MARGIN) { bootloaderIncrementBadEventCounter(UART1_INPUT_BUFFER_ABOVE_SAFETY_MARGIN); }
    } else {
        // ICK! Buffer is full.
        bootloaderIncrementBadEventCounter(UART1_INPUT_BUFFER_OVERFLOW);
        initCraftInputBuffer();
    }
}


// bootloaderIncrementBadEventCounter
//
// It' the same function as on the application project.
// Except it does not an to the autonomous event facility.
//
void bootloaderIncrementBadEventCounter(U16 badEventIndex)
{
    if (badEventIndex < MAX_BAD_EVENTS_COUNTERS) { amuBadEventsCountersBlock[badEventIndex]++;              }
    else                                         { amuBadEventsCountersBlock[INVAL_COUNTER_TO_INCREMENT]++; }
}


/**
 * Apparent
 * User/debug input buffer (CRAFT/J50) initialization. Called by main on system startup,
 * when user input is ready for processing, and when a full buffer is detected.
 * It is the responsibility of the UART1 interrupt service routine to set "ready".
 */
void initCraftInputBuffer(void)
{
    memset((char *)&pCraftInputBuffer[0], 0, CRAFT_INPUT_BUFFER_LENGTH);
    craftInputBufferRank = 0;
    craftInputReady      = FALSE;
}


/**
 * Apparent
 * Application input buffer (SBCCOM/J21) initialization. Called by main on system startup,
 * when application input is ready for processing, and when a full buffer is detected.
 * It is the responsibility of the UART0 interrupt service routine to set "ready".
 */
void initSbcComInputBuffer(void)
{
    memset((char *)&pSbcComInputBuffer[0], 0, SBCCOM_INPUT_BUFFER_LENGTH);
    sbcComInputBufferRank   = 0;
    amuOutputChecksum       = 0;
    charsWrittenByInterrupt = 0;
    sbcComInputReady        = FALSE;
    inputActivelyProcessed  = FALSE;
}


/**
 * Apparent
 * Check if UART0 interrupt handler has set user input as ready to process.
 * If the ready bit is set, then interpret whatever is in the buffer.
 *
 * Currently, no requirements specified for what is expected on this port.
 * For test/feasibility: a few commands are parsed.
 */
void checkSbcComUserInput(void)
{
    if (sbcComInputReady == TRUE) {
        inputActivelyProcessed = TRUE;
        processUart0InputString(pSbcComInputBuffer);
        initSbcComInputBuffer();
    }
}


/**
 * Apparent
 * Check if UART1 interrupt handler has set user input as ready to process.
 * If the ready bit is set, then interpret whatever is in the buffer.
 *
 * THIS FUNCTION: to be customized for development purpose.
 */
void checkCraftUserInput(void)
{
    if (craftInputReady == TRUE) {
        processUart1InputString(pCraftInputBuffer);
        initCraftInputBuffer();  
    }
}


// checkSoftwareReset
//
//
void checkSoftwareReset(void)
{
    if (softwareResetPlease) {
        if ((g_ul_ms_ticks - softwareResetTimestamp) > 10000) {
             executeSoftwareResetAction();
        }            
    }
}


// The static functions of this module:





/**
 * Apparent
 * Init data to support general AMU functionality and features.
 */
static void initApparentData(void)
{
    charsWrittenByInterrupt       = 0;
    totalUartCharsWritten         = 0;
    amuOutputChecksum             = 0;
    sbcComEchoChar                = FALSE;
    inputChecksumRequired         = TRUE;
    softwareResetPlease           = FALSE;
    softwareResetTimestamp        = 0;
}


// executeSoftwareResetAction
//
//
static void executeSoftwareResetAction(void)
{
    __NVIC_SystemReset();
}


// resetFunc
static void resetFunc(void)
{
    // Prepare for the hardware reset:
    // {"CLI":{"COMMAND":"reset","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"RESET":"RESET IN 5 SECONDS"},"RESULT":"SUCCESS"}}
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING,
                                                                COMMAND_STRING,   RESET_CMD,
                                                                ARGUMENTS_STRING, NONE_STRING,
                                                                CODE_STRING,      0,
                                                                RESPONSE_STRING,  RESET_STRING, RESET_IN_5_SECONDS_STRING,
                                                                RESULT_STRING,    SUCCESS_STRING);
    softwareResetPlease    = TRUE;
    softwareResetTimestamp = g_ul_ms_ticks;
}






// displayControllerResetReason
//
// Called to display reset reason info, either as part of the response to the "banner" command
// (so free-format output), or as part of some other command where the output is json-structured.
// Out the following:
//
// - if free-format:     - SAME70 Reset Reason: SOFTWARE ("RESET INFO"=0x00010300 masked=0x00000300)
// - if json-structured: "SAME70 RESET REASON":"SOFTWARE","RESET INFO":"0x00010300 0x300"
//
// This function is identical on the bootloader program, and ought to be in a common module.
//
static void displayControllerResetReason(BOOL jsonYes)
{
    // How about a lookup table, huh?
    if (jsonYes) { printf("%s:", SAME70_RESET_REASON_STRING); } else { printf("-- SAME70 Reset Reason: ");         }
    if      (same70ResetInfoDb.reason == RESET_REASON_GENERAL)  { if (jsonYes) { printf("%s,", RESET_REASON_GENERAL_STRING);  } else { printf("%s ", RESET_REASON_GENERAL_STRING);  } }
    else if (same70ResetInfoDb.reason == RESET_REASON_BACKUP)   { if (jsonYes) { printf("%s,", RESET_REASON_BACKUP_STRING);   } else { printf("%s ", RESET_REASON_BACKUP_STRING);   } }
    else if (same70ResetInfoDb.reason == RESET_REASON_WATCHDOG) { if (jsonYes) { printf("%s,", RESET_REASON_WATCHDOG_STRING); } else { printf("%s ", RESET_REASON_WATCHDOG_STRING); } }
    else if (same70ResetInfoDb.reason == RESET_REASON_SOFTWARE) { if (jsonYes) { printf("%s,", RESET_REASON_SOFTWARE_STRING); } else { printf("%s ", RESET_REASON_SOFTWARE_STRING); } }
    else if (same70ResetInfoDb.reason == RESET_REASON_NRST_LOW) { if (jsonYes) { printf("%s,", RESET_REASON_NRST_LOW_STRING); } else { printf("%s ", RESET_REASON_NRST_LOW_STRING); } }
    else                                                        { if (jsonYes) { printf("%s,", RESET_REASON_UNKNOWN_STRING);  } else { printf("%s ", RESET_REASON_UNKNOWN_STRING);  } }

    if (jsonYes) { printf("%s:\"0x%08lX 0x%0lX\"",           RESET_INFO_STRING, same70ResetInfoDb.status, same70ResetInfoDb.statusMasked); }
    else         { printf("(%s=0x%08lX masked=0x%08lX)\r\n", RESET_INFO_STRING, same70ResetInfoDb.status, same70ResetInfoDb.statusMasked); }
}






// programResponse
//
// print a json-like string of the form:
//
// {"CLI":{"COMMAND":"program","ARGUMENTS":"NONE","CODE":0,"RESPONSE":{"PROGRAM":"BOOTLOADER",
//                                                                     "FLASH LOAD ADDRESS":"0x00580000",
//                                                                     "FLASH SIGNATURES":{"APPLICATION":"0xE0B2B315","BOOTLOADER":"0x3810910F"},
//                                                                     "UPGRADE STATE MACHINE":"IDLE",
//                                                                     "UPGRADE PACKET SIZE":512,
//                                                                     "UPTIME (SECS)":303,
//                                                                     "RELEASE":"14",
//                                                                     "SAME70 RESET REASON":"SOFTWARE","RESET INFO":"0x00010300 0x300"
//                                                                    },"RESULT":"SUCCESS"}}
static void programResponse(void)
{
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s,%s:\"0x%08lX\",%s:{%s:\"0x%08lX\",%s:\"0x%08lX\"},",
           CLI_STRING, COMMAND_STRING,   PROGRAM_CMD,
                       ARGUMENTS_STRING, NONE_STRING,
                       CODE_STRING,      0,
                       RESPONSE_STRING,  PROGRAM_STRING,            BOOTLOADER_STRING,
                                         FLASH_LOAD_ADDRESS_STRING, bootloaderLoadFlashAddress, //PO
                                         FLASH_SIGNATURES_STRING,   APPLICATION_STRING,  applicationFlashSignature, BOOTLOADER_STRING, bootloaderFlashSignature);
    displayUpgradeStateMachineInfo();
    printf(",%s:%u,%s:%lu,%s:\"%s\"", UPGRADE_PACKET_SIZE_STRING, UPGRADE_PACKET_DATA_SIZE_MAX, UPTIME_STRING, g_ul_ms_ticks/1000, RELEASE_STRING, APPARENT_RELEASE_STRING);
    printf(",");
    displayControllerResetReason(JSON_OUTPUT);
    printf("},%s:%s}}\r\n", RESULT_STRING,    SUCCESS_STRING);
}


// getAutonomousEvents
//
// The gateway/iGos will periodically poll the AMU for autonomous events detected by the AMU,
// where the AMU is normally in application mode. As this is bootloader, simply return an
// empty "no events" response string.
//
// {"CLI":{"COMMAND":"get_auto_events","ARGUMENTS":"NONE","CODE":0,"RESPONSE":{"AUTONOMOUS EVENTS LIST":[]},"RESULT":"SUCCESS"}}
//
void getAutonomousEvents(void)
{
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:[]},%s:%s}}\r\n",
            CLI_STRING, COMMAND_STRING,   GET_AUTONOMOUS_EVENTS_CMD,
                        ARGUMENTS_STRING, NONE_STRING,
                        CODE_STRING,      0,
                        RESPONSE_STRING,  AUTONOMOUS_EVENTS_LIST_STRING,
                        RESULT_STRING,    SUCCESS_STRING);
}


// displayFlashSignatures
//
//
//  "FLASH SIGNATURES":{"APPLICATION":"0x12345678","BOOTLOADER":"0x87654321"}
//
static void displayFlashSignatures(void)
{
    printf("%s:{%s:\"0x%08lX\",%s:\"0x%08lX\"}", FLASH_SIGNATURES_STRING, APPLICATION_STRING,  applicationFlashSignature, BOOTLOADER_STRING, bootloaderFlashSignature);
    return;
}


// displayFlashProgramSignatures
//
//
// {"CLI":{"COMMAND":"display_program_signatures","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"FLASH SIGNATURES":{"APPLICATION":"0x12345678","BOOTLOADER":"0x87654321"}},"RESULT":"SUCCESS"}}
//
static void displayFlashProgramSignatures(void)
{
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{", CLI_STRING, COMMAND_STRING, DISPLAY_PROGRAM_SIGNATURES_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, 0, RESPONSE_STRING);
    displayFlashSignatures();
    printf("},%s:%s}}\r\n", RESULT_STRING, SUCCESS_STRING);
}


// helloResponse
//
// {"CLI":{"COMMAND":"hello","ARGUMENTS":"NONE","CODE":0,"RESPONSE":{"PROGRAM":"BOOTLOADER",
//                                                                   "FLASH SIGNATURES":{"APPLICATION":"YYY","BOOTLOADER":"XXX"},
//                                                                   "UPGRADE STATE MACHINE":"IDLE",
//                                                                   "RELEASE":"84"
//                                                                   "UPTIME (SECS)":448653
//                                                                  },"RESULT":"SUCCESS"}}
static void helloResponse(void)
{
    // WARNING: be careful making output formating changes, they could break the AMU-SBC interface.

    // 1: The preamble.
    printf("{%s:{%s:\"%s\",%s:%s,%s:0,%s:{", CLI_STRING, COMMAND_STRING, KEEP_ALIVE_HELLO_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING);

    // 2: Running application.
    printf("%s:%s,", PROGRAM_STRING, BOOTLOADER_STRING);

    //  3: Read the current flash signatures.
    displayFlashSignatures(); printf(",");

    // 4: The upgrade state machine. Add a trailing comma afterwards,
    // in preparation for the next json-like component.
    displayUpgradeStateMachineInfo();
    printf(",");

    // 5: the release
    printf("%s:\"%s\",", RELEASE_STRING, APPARENT_RELEASE_STRING );

    // 6: Uptime.
    printf("%s:%lu", UPTIME_STRING, (g_ul_ms_ticks/1000));

    // Done.
    printf("},%s:%s}}\r\n", RESULT_STRING, SUCCESS_STRING); // close off the json-like string.

    return;
}


// displayApparentMiniHelp
//
//
//
//
//
static void displayApparentMiniHelp(void)
{
    // CLI GROUP 1:
    printf("%25s - maintain login keep-alive\r\n",                                       /*  1 */ KEEP_ALIVE_HELLO_CMD);
    printf("%25s - get the autonomous events\r\n",                                       /*  2 */ GET_AUTONOMOUS_EVENTS_CMD);
    printf("%25s - display and reset the Apparent AMU counters\r\n",                     /*  3 */ COUNTERS_CMD);
    printf("%25s - f/w upgrade: %s %s=<%s|%s|%s|%s|%s> %s=<hex file string data>\r\n",   /*  4 */ UPGRADE_CMD, UPGRADE_CMD,      COMMAND_STRING,
                                                                                                 CLI_ARG_UPGRADE_START_STRING,  CLI_ARG_UPGRADE_DATA_STRING,   CLI_ARG_UPGRADE_END_STRING,
                                                                                                 CLI_ARG_UPGRADE_COMMIT_STRING, CLI_ARG_UPGRADE_CANCEL_STRING, CLI_ARG_UPGRADE_DATA_STRING);
    printf("%25s - software reset\r\n",                                                  /*  5 */ RESET_CMD);
    printf("%25s - display the type of running program (application or bootloader)\r\n", /*  6 */ PROGRAM_CMD);
    printf("%25s - display program flash signatures\r\n",                                /*  7 */ DISPLAY_PROGRAM_SIGNATURES_CMD);
    printf("%25s - display bootloader banner\r\n",                                       /*  8 */ DISPLAY_BOOTLOADER_BANNER_CMD);
    printf("%25s - display help\r\n",                                                    /*  9 */ DISPLAY_HELP_CMD);
    printf("%25s - enable upgrade info debug boolean\r\n",                               /* 10 */ ENABLE_UPGRADE_INFO_CMD);
    printf("%25s - disable upgrade info debug boolean\r\n",                              /* 11 */ DISABLE_UPGRADE_INFO_CMD);
    printf("%25s - enable upgrade HEX info debug boolean\r\n",                           /* 12 */ ENABLE_UPGRADE_HEX_INFO_CMD);
    printf("%25s - disable upgrade HEX info debug boolean\r\n",                          /* 13 */ DISABLE_UPGRADE_HEX_INFO_CMD);
    /* */                                                                                /* 14 */ // Do not mention anything about the set_local_testing command.
    printf("%25s - for test: infinite nop ... watchdog\r\n",                             /* 15 */ NOP_CMD);
    printf("%25s - upgrade testing: drop the next incoming data packet\r\n",             /* 16 */ DROP_UPGRADE_DATA_PACKET_CMD);
    printf("%25s - upgrade testing: drop the next outgoing ACK packet\r\n",              /* 17 */ DROP_UPGRADE_PACKET_ACK_CMD);

#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
    printf("%25s - print the flash holding buffer\r\n",                                  /* 18 */ PRINT_FH_CMD);
    printf("%25s - write a pattern to flash holding buffer:   %s %s=12345678\r\n",       /* 19 */ SET_FH_PATTERN_CMD,    SET_FH_PATTERN_CMD,    CLI_ARG_LONG_STRING);
    printf("%25s - read 1 block from flash to holding buffer: %s %s=40000\r\n",          /* 20 */ READ_FLASH_BLOCK_CMD,  READ_FLASH_BLOCK_CMD,  CLI_ARG_ADDR_STRING);
    printf("%25s - write 1 block of holding buffer to flash:  %s %s=40000\r\n",          /* 21 */ WRITE_FLASH_BLOCK_CMD, WRITE_FLASH_BLOCK_CMD, CLI_ARG_ADDR_STRING);
    printf("%25s - calculate the CRC over the flash holding block\r\n",                  /* 22 */ CRC_FLASH_BLOCK_CMD);
    printf("%25s - test/check if flash memory range is erased: %s %s=20000 %s=10\r\n",   /* 23 */ IS_FLASH_RANGE_ERASED_CMD, IS_FLASH_RANGE_ERASED_CMD, CLI_ARG_ADDR_STRING, CLI_ARG_LENGTH_STRING);
    printf("%25s - test/read up to 32 longs from flash: %s %s=20000 %s=10\r\n",          /* 24 */ READ_FLASH_LONG_CMD,    READ_FLASH_LONG_CMD,    CLI_ARG_ADDR_STRING, CLI_ARG_LENGTH_STRING);
    printf("%25s - test/write 1 long to flash: %s %s=20000 %s=E0B2B315\r\n",             /* 25 */ WRITE_FLASH_LONG_CMD,   WRITE_FLASH_LONG_CMD,   CLI_ARG_ADDR_STRING, CLI_ARG_LONG_STRING);
#else
// Make no mention of the flash-centric utilities.
#endif

    printf("END OF HELP\r\n"); // <- do this last, for prettier output.
}


// executeNOP
//
// Go into a do-absolutely nothing infinite loop, to see if the watchdog will BARK!
//
static void executeNOP(void)
{

    printf("DO NOP\r\n");
    while(1) { __ASM volatile ("nop"); }    //__NOP;
    printf("you should never see this ...\r\n");
}


static void processUart0InputString(char *pUserInput)
{
    // This "parser" is bare-bones, un-glamorous, not even pretty.
    //
    // One day, this function will grow into something we can all be proud of. Depending on compile options,
    // user input may or may not contain the end-of-input trailing signature with checksum. If disabled, the
    // presence of the signature has no effect on input analysis.

    // BUG NOTICE (not worth fixing until a proper parser is implemented (if ever):
    //
    // Issue: the user can enter a command such as "loginnnnn password" and the command
    //        is validated correctly as the "login" command, since the 1st set of characters
    //        is a match for one of the set of allowed commands.

    if (strlen(pUserInput) > 0) {
        if (isInputChecksumValid(pUserInput)) {
            // The input checksum was OK. Process the user input.
            printf("\r\n"); // forces all subsequent output to start on a new line.

            // CLI Group 1:
                 if (strncmp(pUserInput, KEEP_ALIVE_HELLO_CMD,                 strlen(KEEP_ALIVE_HELLO_CMD)) == 0)               { helloResponse();                                                        displayEndOfOutput(); } //  1
            else if (strncmp(pUserInput, GET_AUTONOMOUS_EVENTS_CMD,            strlen(GET_AUTONOMOUS_EVENTS_CMD)) == 0)          { getAutonomousEvents();                                                  displayEndOfOutput(); } //  2
            else if (strncmp(pUserInput, COUNTERS_CMD,                         strlen(COUNTERS_CMD)) == 0)                       { displayAndResetBadEventsCounters();                                     displayEndOfOutput(); } //  3
            else if (strncmp(pUserInput, UPGRADE_CMD,                          strlen(UPGRADE_CMD)) == 0)                        { processUpgradeCommand(pUserInput);                                      displayEndOfOutput(); } //  4
            else if (strncmp(pUserInput, RESET_CMD,                            strlen(RESET_CMD)) == 0)                          { resetFunc();                                                            displayEndOfOutput(); } //  5
            else if (strncmp(pUserInput, PROGRAM_CMD,                          strlen(PROGRAM_CMD)) == 0)                        { programResponse();                                                      displayEndOfOutput(); } //  6
            else if (strncmp(pUserInput, DISPLAY_PROGRAM_SIGNATURES_CMD,       strlen(DISPLAY_PROGRAM_SIGNATURES_CMD)) == 0)     { displayFlashProgramSignatures();                                        displayEndOfOutput(); } //  7
            
            // CLI Group 2:
            else if (strncmp(pUserInput, DISPLAY_BOOTLOADER_BANNER_CMD,        strlen(DISPLAY_BOOTLOADER_BANNER_CMD)) == 0)      { displayApparentBootLoaderBanner();                                      displayEndOfOutput(); } //  8
            else if (strncmp(pUserInput, DISPLAY_HELP_CMD,                     strlen(DISPLAY_HELP_CMD)) == 0)                   { displayApparentMiniHelp();                                              displayEndOfOutput(); } //  9
            else if (strncmp(pUserInput, ENABLE_UPGRADE_INFO_CMD,              strlen(ENABLE_UPGRADE_INFO_CMD)) == 0)            { upgradeInfoOutputOnOff(TRUE);                                           displayEndOfOutput(); } // 10
            else if (strncmp(pUserInput, DISABLE_UPGRADE_INFO_CMD,             strlen(DISABLE_UPGRADE_INFO_CMD)) == 0)           { upgradeInfoOutputOnOff(FALSE);                                          displayEndOfOutput(); } // 11
            else if (strncmp(pUserInput, ENABLE_UPGRADE_HEX_INFO_CMD,          strlen(ENABLE_UPGRADE_HEX_INFO_CMD)) == 0)        { upgradeHexInfoOutputOnOff(TRUE);                                        displayEndOfOutput(); } // 12
            else if (strncmp(pUserInput, DISABLE_UPGRADE_HEX_INFO_CMD,         strlen(DISABLE_UPGRADE_HEX_INFO_CMD)) == 0)       { upgradeHexInfoOutputOnOff(FALSE);                                       displayEndOfOutput(); } // 13
            else if (strncmp(pUserInput, SET_LOCAL_TESTING_CMD,                strlen(SET_LOCAL_TESTING_CMD)) == 0)              {                                                                                               } // 14
            else if (strncmp(pUserInput, NOP_CMD,                              strlen(NOP_CMD)) == 0)                            {  executeNOP();                                                          displayEndOfOutput(); } // 15
            else if (strncmp(pUserInput, DROP_UPGRADE_DATA_PACKET_CMD,         strlen(DROP_UPGRADE_DATA_PACKET_CMD)) == 0)       { dropUpgradeSomething(PLEASE_DROP_THE_NEXT_DATA_PACKET);                 displayEndOfOutput(); } // 16
            else if (strncmp(pUserInput, DROP_UPGRADE_PACKET_ACK_CMD,          strlen(DROP_UPGRADE_PACKET_ACK_CMD)) == 0)        { dropUpgradeSomething(PLEASE_DROP_THE_NEXT_PACKET_ACK);                  displayEndOfOutput(); } // 17

#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
            else if (strncmp(pUserInput, PRINT_FH_CMD,                         strlen(PRINT_FH_CMD)) == 0)                       { printFlashHoldingBuffer(); /* in flashinator.c */                       displayEndOfOutput(); } // 18
            else if (strncmp(pUserInput, SET_FH_PATTERN_CMD,                   strlen(SET_FH_PATTERN_CMD)) == 0)                 { setFlashHoldingPattern(pUserInput);                                     displayEndOfOutput(); } // 19
            else if (strncmp(pUserInput, READ_FLASH_BLOCK_CMD,                 strlen(READ_FLASH_BLOCK_CMD)) == 0)               { readFromFlashMemoryIntoHolding(pUserInput);                             displayEndOfOutput(); } // 20
            else if (strncmp(pUserInput, WRITE_FLASH_BLOCK_CMD,                strlen(WRITE_FLASH_BLOCK_CMD)) == 0)              { writeFromHoldingIntoFlashMemory(pUserInput);                            displayEndOfOutput(); } // 21
            else if (strncmp(pUserInput, CRC_FLASH_BLOCK_CMD,                  strlen(CRC_FLASH_BLOCK_CMD)) == 0)                { calculateAndPrintHoldingBlockCrc();                                     displayEndOfOutput(); } // 22
            else if (strncmp(pUserInput, IS_FLASH_RANGE_ERASED_CMD,            strlen(IS_FLASH_RANGE_ERASED_CMD)) == 0)          { isCheckFlashErasedTestCommand(pUserInput);                              displayEndOfOutput(); } // 23
            else if (strncmp(pUserInput, READ_FLASH_LONG_CMD,                  strlen(READ_FLASH_LONG_CMD)) == 0)                { readFlashCommand(pUserInput);                                           displayEndOfOutput(); } // 24
            else if (strncmp(pUserInput, WRITE_FLASH_LONG_CMD,                 strlen(WRITE_FLASH_LONG_CMD)) == 0)               { writeFlashLongCommand(pUserInput);                                      displayEndOfOutput(); } // 25
#else
            // These are not mentioned, invalid command will be returned.
            //else if (strncmp(pUserInput, PRINT_FH_CMD...                       // 17
            //else if (strncmp(pUserInput, SET_FH_PATTERN_CMD...                 // 18
            //else if (strncmp(pUserInput, READ_FLASH_BLOCK_CMD...               // 19
            //else if (strncmp(pUserInput, WRITE_FLASH_BLOCK_CMD...              // 20
            //else if (strncmp(pUserInput, CRC_FLASH_BLOCK_CMD...                // 21
            //else if (strncmp(pUserInput, IS_FLASH_RANGE_ERASED_CMD...          // 22
            //else if (strncmp(pUserInput, READ_FLASH_LONG_CMD...                // 23
            //else if (strncmp(pUserInput, WRITE_FLASH_LONG_CMD...               // 24
#endif // INCLUDE_CLI_FLASH_COMMANDS

            else {   printf("{%s:{%s:\"%s\",%s:%s,%s:1,%s:%s,%s:%s}}\r\n",
                            CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, INVALID_COMMAND_STRING, RESULT_STRING, FAIL_STRING);                      displayEndOfOutput(); }
        } else {
            // Checksum did not pass muster. COUNTER???
        }
    } else {
        // Empty buffer. Normally don't come here unless the user entered a plain <CR> that is echo'd locally.
        printf("\r\n");
    }
}





#if 0 // RGB LED ACTIVITY

// updateRgbLedActivityStatus
//
// This function is implemented on both the APPLICATION and BOOTLOADER projects,
// and is expected to be identical on both projects. It is tailored to account for
// application or bootloader mode, with compilation controlled by a switch in the
// apparent.h header file.
//
// The RGB RED LED activity status is set to go on/off according to the following cadence:
//
// - if application:     ON 0.250secs OFF 3.0secs    <- a blink every few seconds: calming, reassuring
// - if bootloader/idle: ON 1.5secs   OFF 1.5secs    <- regular ON/OFF, not too fast, distinct from application cadence
// - if bootloader/XFER: ON 0.250secs OFF 0.250secs  <- relatively fast, regular blinking during file transfer
//
void updateRgbLedActivityStatus(void)
{
#ifdef AMU_BOOTLOADER

    // When in BOOTLOADER mode: cadence for the RED LED activity status indicator depends
    //                          on the firmware upgrade state machine state.
    if (g_ul_ms_ticks > ledActivityCb.nextTimestamp) {
        if (ledActivityCb.nextAction == RGB_ACTIVITY_OFF) {
            // Turn the LED to OFF for the required duration of time.
            // Prepare to turn it ON when this period expires.
            setRgbLed(RGB_LED_RED_GPIO, IOPORT_DIR_INPUT, IOPORT_PIN_LEVEL_LOW);
            ledActivityCb.nextAction = RGB_ACTIVITY_ON;
            if (isUpgradeStateMachineIdle()) { ledActivityCb.nextTimestamp = g_ul_ms_ticks + BOOTLOADER_IDLE_OFF_PERIOD; }
            else                             { ledActivityCb.nextTimestamp = g_ul_ms_ticks + BOOTLOADER_XFER_OFF_PERIOD; }
        } else {
            // Turn the LED to ON for the required duration of time.
            // Prepare to turn it OFF when this period expires.
            setRgbLed(RGB_LED_RED_GPIO, IOPORT_DIR_OUTPUT, IOPORT_PIN_LEVEL_HIGH);
            ledActivityCb.nextAction = RGB_ACTIVITY_OFF;
            if (isUpgradeStateMachineIdle()) { ledActivityCb.nextTimestamp = g_ul_ms_ticks + BOOTLOADER_IDLE_ON_PERIOD; }
            else                             { ledActivityCb.nextTimestamp = g_ul_ms_ticks + BOOTLOADER_XFER_ON_PERIOD; }
        }
    }

#elif AMU_APPLICATION

    if (rgbLedFlashingDemoCb.flashingEnabled == FALSE) {
        // When in APPLICATION mode: only 1 cadence for the RED LED activity status indicator.
        if (g_ul_ms_ticks > ledActivityCb.nextTimestamp) {
            if (ledActivityCb.nextAction == RGB_ACTIVITY_OFF) {
                // Turn the LED to OFF for the required duration of time.
                // Prepare to turn it ON when this period expires.
                setRgbLed(RGB_LED_RED_GPIO, IOPORT_DIR_INPUT, IOPORT_PIN_LEVEL_LOW);
                ledActivityCb.nextTimestamp = g_ul_ms_ticks + APPLICATION_LED_OFF_PERIOD;
                ledActivityCb.nextAction = RGB_ACTIVITY_ON;
                } else {
                // Turn the LED to ON for the required duration of time.
                // Prepare to turn it OFF when this period expires.
                setRgbLed(RGB_LED_RED_GPIO, IOPORT_DIR_OUTPUT, IOPORT_PIN_LEVEL_HIGH);
                ledActivityCb.nextTimestamp = g_ul_ms_ticks + APPLICATION_LED_ON_PERIOD;
                ledActivityCb.nextAction = RGB_ACTIVITY_OFF;
            }
        }
    }

#else
    I am a big fat compile error
#endif

    return;
}


// setRgbLed
//
// Low-level function to set any of the RED, GREEN or BLUE LED to MEDIUM, OFF or BRILLIANT intensity.
// In a running system, be it APPLICATION or BOOTLOADER, this function is called at the appropriate
// interval to set the RED LED to either MEDIUM or OFF.
static void setRgbLed(U32 gpio, U32 direction, U32 level)
{
    ioport_set_pin_dir(gpio,   direction);
    ioport_set_pin_level(gpio, level);
}



#endif




static void processUart1InputString(char *pUserInput)
{
    if (strlen(pUserInput) > 0) {
        usart_serial_write_packet((usart_if)UART1, (uint8_t *)"\r\nSTRING TO PROCESS: ", 21);
        usart_serial_write_packet((usart_if)UART1, (uint8_t *)pUserInput,                strlen(pUserInput));
        usart_serial_write_packet((usart_if)UART1, (uint8_t *)"\r\n",                    2);
    } else {
        // Empty buffer. Normally don't come here unless the user entered a plain <CR> that is echo'd locally.
        printf("\r\n");
    }
}


/**
 * Apparent
 * Display the apparent banner.
 *
 */
void displayApparentBootLoaderBanner(void)
{
    // Output the system header.
    printf(APPARENT_SYSTEM_BOOTLOADER_HEADER);

    // How long has the system been running?
    printf("-- UPTIME/SECS: %lu\r\n", (g_ul_ms_ticks/1000));

    // Output the application's version.
    printf("%s%s\r\n", APPARENT_RELEASE_TITLE_STRING, APPARENT_RELEASE_STRING);

    // Output from where in flash this bootloader is running.
    printf("-- Bootloader in flash at 0x%08lX\r\n", bootloaderLoadFlashAddress);

    displayControllerResetReason(NO_JSON_OUTPUT);
}


// readOneProgramFlashSignature
//
//
//
static U32 readOneProgramFlashSignature(U32 baseMemoryAddress)
{
    U32  instructionWord = 0xFFFFFFFF;
    U32 *instructionWordPtr;
    U32  flashResult;

    flashResult = flash_set_wait_state(baseMemoryAddress, 6);
    if (flashResult != FLASH_RC_OK) {  printf("READ 1 FLASH PROGRAM SIGNATURE FROM 0x%08lX FAIL - flashResult=0x%08lX\r\n", baseMemoryAddress, flashResult); }

    // Point to the specified flash memory base address to start blank-checking from.
    // Pointer is of type 32-bit (Instruction Words).
    instructionWordPtr = (uint32_t *) baseMemoryAddress;

    // Read the current instruction word (IW)
    cpu_irq_disable();
    SCB_DisableICache();
    SCB_DisableDCache();
    instructionWord = *instructionWordPtr;
    SCB_EnableDCache();
    SCB_EnableICache();
    cpu_irq_enable();

    return instructionWord;
}


// readOutProgramFlashSignatures
//
//
void readOutProgramFlashSignatures(void)
{
    applicationFlashSignature = readOneProgramFlashSignature(APPLICATION_SIGNATURE_ADDRESS);
    bootloaderFlashSignature  = readOneProgramFlashSignature(BOOTLOADER_SIGNATURE_ADDRESS);
}


#if INCLUDE_CLI_FLASH_COMMANDS == TRUE

// setFlashHoldingPattern
//
//
//
//
static void setFlashHoldingPattern(char *pUserInput)
{
    char  *pArguments;
    char   pLongString[32];
    U32    someLong;

    pArguments = &pUserInput[strlen(SET_FH_PATTERN_CMD) + 1];
    memset(pLongString, 0, 32);
    findComponentStringData(CLI_ARG_LONG_STRING, pLongString, pArguments);
    if (newATOH(pLongString, &someLong)) {
        printf("-> set pattern in flash holding with 0x%08lX\r\n", someLong);
        writeFlashHoldingPattern(someLong);
    } else {
        printf("... BUT LONG %s DID NOT CONVERT TO HEX\r\n", pLongString);
    }
}


// readFromFlashMemoryIntoHolding
//
// Read some flash memory into the holding block.
//
//
static void readFromFlashMemoryIntoHolding(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[32];
    U32    flashAddress;
    BOOL   readResult;

    pArguments = &pUserInput[strlen(READ_FLASH_BLOCK_CMD) + 1];
    memset(pAddressString, 0, 32);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    if (newATOH(pAddressString, &flashAddress)) {
        printf("-> read 1 block of flash from address 0x%08lX to the holding block ... ", flashAddress);
        readResult = readFlashToHoldingBlock(flashAddress);
        if (readResult) { printf("DONE\r\n");     }
        else            { printf("*FAILED*\r\n"); }
    } else {
        printf("... BUT ADDRESS %s DID NOT CONVERT TO HEX\r\n", pAddressString);
    }
}


// writeHoldingBlockToFlash
//
// The contents of the flash holding buffer will be written to flash at the specified flash address.
// The write will fail if the address is not within bounds.
//
static void writeFromHoldingIntoFlashMemory(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[32];
    U32    flashAddress;
    BOOL   writeResult;

    pArguments = &pUserInput[strlen(WRITE_FLASH_BLOCK_CMD) + 1];
    memset(pAddressString, 0, 32);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    if (newATOH(pAddressString, &flashAddress)) {
        printf("-> write holding buffer to flash address  0x%08lX ... ", flashAddress);
        writeResult = writeHoldingBlockToFlash(flashAddress);
        if (writeResult) { printf("DONE\r\n");     }
        else             { printf("*FAILED*\r\n"); }
    } else {
        printf("... BUT %s DID NOT CONVERT TO HEX\r\n", pAddressString);
    }
}


// calculateHoldingBlockCrc
//
//
static void calculateAndPrintHoldingBlockCrc(void)
{
    U32 crc;
    crc = calculateHoldingBlockCrc();
    printf("calculated holding block CRC=0x%08lX\r\n", crc);
}


// isCheckFlashErasedTestCommand
//
// Command takes the following form:
//
// read_flash addr=xxxx len=xxxx
//
//
//
static void isCheckFlashErasedTestCommand(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[32];
    char   pLengthString[32];
    U32    flashAddress;
    U32    flashLength;

    pArguments = &pUserInput[strlen(IS_FLASH_RANGE_ERASED_CMD) + 1];
    memset(pAddressString, 0, 32);
    memset(pLengthString,  0, 32);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_LENGTH_STRING,  pLengthString,  pArguments);

    if ((strlen(pAddressString) == 0) || (strlen(pLengthString) == 0)) {
        printf("addr and/or len not found\r\n");
    } else {
        flashAddress = atoh(pAddressString);
        flashLength  = atoh(pLengthString);
        printf("CHECK %lu LONGS OF FLASH FROM 0x%08lX: ", flashLength, flashAddress);
        if (isFlashErasedMemoryRange(flashAddress, flashLength)) { printf("FLASH IS ERASED\r\n");       }
        else                                                     { printf("FLASH IS *NOT* ERASED\r\n"); }
    }
}


// readFlashCommand
//
// Command takes the following form:
//
// read_flash addr=xxxx len=xxxx
//
//
//
static void readFlashCommand(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pLengthString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    flashAddress;
    U32    flashLength;

    pArguments = &pUserInput[strlen(READ_FLASH_LONG_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pLengthString,  0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_LENGTH_STRING,  pLengthString,  pArguments);

    if ((strlen(pAddressString) == 0) || (strlen(pLengthString) == 0)) {
        printf("addr and/or len not found\r\n");
    } else {
        flashAddress = atoh(pAddressString);
        flashLength  = atoh(pLengthString);
        printf("WILL READ %lu BYTES OF FLASH FROM 0x%08lX \r\n", flashLength, flashAddress);
        readFlashMemoryRange(flashAddress, flashLength);
    }
}


// writeFlashLongCommand
////
//
// - flash_init
// - flash_unlock
// - flash_erase_sector
// - flash_write
// - flash_lock
//
//
// Command takes the following form:
//
// write_flash addr=xxxx long=xxxx
//
//
//
static void writeFlashLongCommand(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pLongString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    flashAddress;
    U32    flashAddressActualStart;
    U32    flashAddressActualEnd;
    U32    flashLong;
    U32    flashResult;

    pArguments = &pUserInput[strlen(WRITE_FLASH_LONG_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pLongString,    0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_LONG_STRING,    pLongString,    pArguments);

    if ((strlen(pAddressString) == 0) || (strlen(pLongString) == 0)) {
        printf("addr and/or long not found\r\n");
    } else {
        flashAddress = atoh(pAddressString) + IFLASH_ADDR;
        flashLong    = atoh(pLongString);
        flashAddressActualStart = 0;
        flashAddressActualEnd   = 0;
        printf("- write 0x%08lX to flash address 0x%08lX \r\n", flashLong, flashAddress);
        flash_init(FLASH_ACCESS_MODE_128, 6);

        flashResult = flash_unlock(flashAddress, (flashAddress + IFLASH_PAGE_SIZE), &flashAddressActualStart, &flashAddressActualEnd);
        if (flashResult != FLASH_RC_OK) { printf("**UNLOCK [512] FAILED: RC=0x%lX\r\n", flashResult); return;                                                                   }
        else                            { printf("- unlock OK: flashAddressActualStart=0x%lX flashAddressActualEnd=0x%lX\r\n", flashAddressActualStart, flashAddressActualEnd); }

        flashResult = flash_erase_page(flashAddress, IFLASH_ERASE_PAGES_16);
        if (flashResult != FLASH_RC_OK) { printf("***ERASE PAGE FAILED: RC=0x%lX***\r\n", flashResult); return;   }
        else                            { printf("- erase page: OK\r\n");                                         }

        flashResult = flash_write(flashAddress, &flashLong, sizeof(U32), FALSE);
        if (flashResult != FLASH_RC_OK) { printf("***FLASH WRITE FAILED: RC=0x%lX***\r\n", flashResult); return;  }
        else                            { printf("- flash write: OK\r\n");                                        }

        flashResult = flash_lock(flashAddress, (flashAddress + IFLASH_PAGE_SIZE), NULL, NULL);
        if (flashResult != FLASH_RC_OK) { printf("***LOCK FAILED: RC=0x%lX***\r\n", flashResult); return;         }
        else                            { printf("- flash lock: OK\r\n");                                         }

        // Check the security bit: - retval 1 If the security bit is currently set.
        //                         - retval 0 If the security bit is currently cleared.
        //                         - otherwise returns an error code.
        flashResult = flash_is_security_bit_enabled();
        if (flashResult == FLASH_RC_OK) {
            printf("- security bit *must* be enabled\r\n");
            flashResult = flash_enable_security_bit();
            if (flashResult != FLASH_RC_OK) { printf("***ENABLE SECURITY FAILED: RC=0x%lX***\r\n", flashResult); }
            else                            { printf("- enable security: OK\r\n");                               }
        } else { printf("- security bit was enabled\r\n"); }
        printf("- flash write: DONE\r\n");
    }
}


// isFlashErasedMemoryRange
//
// Checks a range of FLASH memory: if any DWORD is not 0xFFFFFFFF then the memory range
//                                 is considered NOT ERASED.
//
// This piece of code taken from some person going by the name of Ken Pergola/Microchip forum.
//
static BOOL isFlashErasedMemoryRange(U32 baseMemoryAddress, U32 numberOfInstructionWords)
{
    BOOL isErased = TRUE;
    U32  instructionWord;
    U32 *instructionWordPtr;

    // Point to the specified flash memory base address to start blank-checking from.
    // Pointer is of type 32-bit (Instruction Words).
    instructionWordPtr = (U32 *)baseMemoryAddress;

    // Range-check argument
    if (numberOfInstructionWords == 0) {
        isErased = FALSE;
        } else {
        // Blank check the number of instruction words (IW) specified
        // (Bail out early on first non-blank instruction word found.)
        while (numberOfInstructionWords-- != 0) {
            // Read the current instruction word (IW)
            instructionWord = *instructionWordPtr++;

            // Is the current instruction word read not erased?
            if (instructionWord != 0xFFFFFFFF) {
                // It is not erased -- bail out
                isErased = FALSE;
                break;
            }
        }
    }
    return isErased;
}


// From Ken Pergola/Microchip forum.
static void readFlashMemoryRange(U32 baseMemoryAddress, U32 numberOfInstructionWords)
{
    U32  flashResult;
    U32  instructionWord;
    U32 *instructionWordPtr;

    // Point to the specified flash memory base address to start blank-checking from.
    // Pointer is of type 32-bit (Instruction Words).
    instructionWordPtr = (uint32_t *) baseMemoryAddress;

    flashResult = flash_set_wait_state(baseMemoryAddress, 6);
    if (flashResult != FLASH_RC_OK) { printf("YAYAYA\r\n"); }

    // Range-check argument
    if (numberOfInstructionWords > 0) {
        while (numberOfInstructionWords-- != 0) {
            // Read the current instruction word (IW)
            instructionWord = 0xFFFFFFFF;
            cpu_irq_disable();
            SCB_DisableICache();
            SCB_DisableDCache();
            instructionWord = *instructionWordPtr++;
            SCB_EnableDCache();
            SCB_EnableICache();
            cpu_irq_enable();
            printf("%08lX ", instructionWord);
        }
        printf("\r\n");
    }
    return;
}


#endif // INCLUDE_CLI_FLASH_COMMANDS


// pokeWatchdog
//
// Restart the SAME70 watchdog timer every so often. Called from main, but also any "controlled" loop where protection
// against a barking watchdog is required. How often? Well, at least more often than its max period of 16 seconds.
// Originally, the watchdog was restarted once every 5 seconds based on a local tick timer. But as checking the timer
// was as onerous as restarting the watchdog anyway, the timer was removed altogether.
//
// During chamber testing: at low temperature (in the -20C range), a WATCHDOG reset was observed. The system would not
// run for more than a few minutes before the SAME70 would reset, reason: WATCHDOG. But only at low temperature during
// chamber testing. Never seen at room temperature. Code inspection, extra loop (for, while) protection added, the low
// temperature WATCHDOG persisted.
//
// What todo, what to do ...
//
// The SAME70 errata data sheet was unearthed, and of the several bugs/workarounds that were described, the most
// applicable bug was that on page 21, section 2.22.1"Watchdog Reset":
//
//
//     Problem description: "With External Reset Length set to 0 (MR.ERSTL= 0) in the Reset Controller Mode register,
//                          a Watchdog Reset may cause an Infinite Reset loop."
//
//     Recommended workaround: "To ensure a correct Watchdog Reset of the system, the ERSTL field in the
//                             Reset Controller Mode register must be set to a non-zero value ( MR.ERSTL >= 1)."
//
//
// The Apparent application fix to prevent WATCHDOG reset at low temperature consists of writing 1 to the aforementioned
// ERSTL register in the reset controller's "mode" register. However, the statement:
//
//  RSTC->RSTC_MR = RSTC_MR_ERSTL(1);
//
// ... had no effect on system behavior. It has been removed.
//
//
void pokeWatchdog(void)
{
    WDT->WDT_CR   = WATCHDOG_CONTROL_RESTART;
    // RSTC->RSTC_MR = RSTC_MR_ERSTL(1); <- did not change system behavior; did not prevent WATCHDOG at low temperaure.
}


#if 0 // This function no longer used, because during application code startup, the
      // SAME70 CPU freezes in the middle of the configuration of the secondary
      // KSZ8795 ethernet controller. The most likely culprit is that something is
      // left "unclean" by the code in this function. The effort to search and fix
      // is significantly more than doing a software reset.

// executeJumpToAppActions
//
//
//
//
static void executeJumpToAppActions(void)
{
    U32 appStartAddress = APPARENT_APPLICATION_ADDRESS;
    U32 appStartAddressResetVector;
    void (*application_code_entry)(void); // Pointer to the Application Section

    // Disable the instruction and data cache.
    SCB_DisableICache();
    SCB_DisableDCache();

    // Set the reset vector for the jump.
    appStartAddressResetVector = appStartAddress + (U32)0x00000004;

    __DSB();                                                        // data synchronization barrier
    __ISB();                                                        // instruction synchronization barrier
    SCB->VTOR = ((uint32_t) appStartAddress & SCB_VTOR_TBLOFF_Msk); // Rebase the vector table base address
    __set_MSP(*(uint32_t *) appStartAddress);                       // Rebase the Stack Pointer
    __DSB();                                                        // data synchronization barrier
    __ISB();                                                        // instruction synchronization barrier

    // Load the Reset Handler address of the application.
    application_code_entry =  (void (*)(void))(unsigned *)(*(unsigned *)(appStartAddressResetVector));
    application_code_entry(); // <- this is it: jump to application's Reset Handler!!!
}
#endif

// processUpgradeCommand
//
// Command is of the form:
//
// upgrade command=<start|data|end|cancel> data=<hex file text data>
//
// The response if json-like:
//
// {"CLI":{"COMMAND":"upgrade","ARGUMENTS":"if any","CODE":0,"RESPONSE":{"somewhat free-form"},"RESULT":"SUCCESS"}}
//
static void processUpgradeCommand(char *pUserInput)
{
    char  *pArguments;
    U32    argumentLength;
    char   pCommandString[32];
    U32    upgradeCommand = UPGRADE_COMMAND_INVALID;
    U32    iggy;
    U32    dataEqualLength = strlen(CLI_ARG_DATA_EQUAL_STRING);
    U32    endOfInputRank;
    U32    startOfInputRank = 0;

    pArguments = &pUserInput[strlen(UPGRADE_CMD) + 1];
    memset(pCommandString,   0, 32);
    
    findComponentStringData(CLI_ARG_COMMAND_STRING,   pCommandString,   pArguments);
    if (strlen(pCommandString) == 0) {
        printf("{%s:{%s:\"%s\",%s:%s;%s:99,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, CLI_COMMAND_EQUAL_NOT_FOUND_STRING, RESULT_STRING, FAIL_STRING);
    } else {
        // Check the actual command.
        if (strncmp(pCommandString, CLI_ARG_UPGRADE_START_STRING, strlen(CLI_ARG_UPGRADE_START_STRING)) == 0) {
            upgradeCommand = UPGRADE_COMMAND_START;
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_DATA_STRING, strlen(CLI_ARG_UPGRADE_DATA_STRING)) == 0) {
            // A data packet. Evaluate the pointer to the very start of the actual text-based data.
            // Start by parsing the packet for "data=" in the user input.

            // It is less than desirable to keep this bit of test code permanently in the build.
			// Since there is no harm including it, then this debug test capability is always available.
			//
			// To exclude a chunk of code from the compilation surrounded by "#ifdef DEBUG_UPGRADE blah_blah_blah #endif",
            // do the following:
            //    - Project -> AMU Properties
            //    - Toolchain -> ARM/GNU C Compiler -> Symbols
            //    - click the green + button and add DEBUG_UPGRADE  in the pop-up box
            //    - click OK
            //    - SAVE the "AMU" properties
            //    - and Bob's your uncle

            if (debugDropTheNextIncomingDataPacket) { debugDropTheNextIncomingDataPacket = FALSE; return; }

            argumentLength = strlen(pArguments);
            for (iggy = 0; iggy < argumentLength; iggy++) {
                if (iggy == (argumentLength + 1)) {
                    // Gone too far without finding the component
                    printf("{%s:{%s:\"%s\",%s:%s,%s:1,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, CLI_DATA_FIELD_TOO_LONG_STRING, RESULT_STRING, FAIL_STRING);
                    break;
                }
                if (strncmp(CLI_ARG_DATA_EQUAL_STRING, &pArguments[iggy], dataEqualLength) == 0) {
                    // Found the data= component in the user input string. Copy the string to the xfer packet data string,
                    // omitting any trailing END-OF-INPUT component. Find the rank of the " END-OF-INPUT 0x00000852 21"
                    // string.
                    endOfInputRank   = findEndOfInputRank(pUserInput);
                    if (endOfInputRank == 0) {
                        endOfInputRank = strlen(pUserInput); // the END-OF-INPUT signature was not included - which is accepted. BUT I DON'T RECALL WHY!!!
                    }
                    startOfInputRank = strlen(UPGRADE_CMD) + 1 + iggy + dataEqualLength;
                    upgradeCommand = UPGRADE_COMMAND_DATA;
                    // The standard SBC/COMM input buffer is no longer require, and will now be treated as a single
                    // packet with a variable number of HEX records. The last character of this packet must be set
                    // to the NULL CHR, else the packet will be rejected.
                    pUserInput[endOfInputRank] = 0;
                    break;
                }                
            } // for ...
            if (startOfInputRank == 0x00) {
                printf("{%s:{%s:\"%s\",%s:%s,%s:1,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, CLI_DATA_EQUAL_NOT_FOUND_STRING, RESULT_STRING, FAIL_STRING);
            }
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_END_STRING, strlen(CLI_ARG_UPGRADE_END_STRING)) == 0) {
            upgradeCommand = UPGRADE_COMMAND_END;
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_COMMIT_STRING, strlen(CLI_ARG_UPGRADE_COMMIT_STRING)) == 0) {
            upgradeCommand = UPGRADE_COMMAND_COMMIT;
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_CANCEL_STRING, strlen(CLI_ARG_UPGRADE_CANCEL_STRING)) == 0) {
            upgradeCommand = UPGRADE_COMMAND_CANCEL;
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_STATS_STRING, strlen(CLI_ARG_UPGRADE_STATS_STRING)) == 0) {
            upgradeCommand = UPGRADE_COMMAND_STATS;
        } else {
            printf("{%s:{%s:\"%s\",%s:%s,%s:1,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, CLI_INVALID_UPGRADE_COMMAND_STRING, RESULT_STRING, FAIL_STRING);
        }            
    }

    if (upgradeCommand == UPGRADE_COMMAND_STATS) {
        displayUpgradeStats();
    } else if (upgradeCommand != UPGRADE_COMMAND_INVALID) {
        // Send this packet to the flashinator state machine for processing.
        upgradeStateMachine(upgradeCommand, &pUserInput[startOfInputRank]);
    }

    return;
}


/**
 * Apparent
 * displayEndOfOutput: for the SBC/Gateway to detect end-of-output. This function appends the
 *                     checksum and number of bytes over which the checksum is calculated.
 *
 */
void displayEndOfOutput(void)
{
    if (inputChecksumRequired) { printf("%s 0x%08lX %d\r\n", END_OF_OUTPUT_SIGNATURE_STRING, amuOutputChecksum, charsWrittenByInterrupt); }
    charsWrittenByInterrupt = 0;
    amuOutputChecksum       = 0;
}


// toggleChecksumRequired
//
static void toggleChecksumRequired(void)
{
    inputChecksumRequired ^= TRUE;
    sbcComEchoChar        ^= TRUE;
}


/**
 * Apparent: isInputChecksumValid
 *
 * User input at the SBC-COM interface must have a trailing checksum signature consisting of a
 * "END-OF-INPUT" set of characters, following by the simple checksum value of the user's input
 * (0x1234 format) and by the number of characters (decimal) over which the checksum is to be
 * evaluated.
 *
 * Example of what a command might look like:
 *
 * beamup scotty other stuff END-OF-INPUT 0x000009CA 25
 *                           ^                        ^
 *                           | <----  signature  ---->|
 *
 *
 */
static BOOL isInputChecksumValid(char *pUserInput) {
    BOOL  checksumValid = FALSE;
    char *pSignature;
    char  pChecksumString[10];
    char *pNumChecksumDigits;
    U16   numChecksumDigits;
    U32   extractedCheckSum;
    U32   calculatedChecksum;
    U32   iggy;
    U32   checksumRank;

    // Secret back door to get in the front door. Disable the CHECKSUM REQUIRED flag. For localized testing.
    if (strncmp(pUserInput, SET_LOCAL_TESTING_CMD, strlen(SET_LOCAL_TESTING_CMD)) == 0) { toggleChecksumRequired(); return TRUE; }

    // Otherwise, input checksum needs to be checked.
    if (inputChecksumRequired) {
        pSignature = pApparent_strstr(pUserInput, (char *)&END_OF_INPUT_SIGNATURE_STRING);
        if (pSignature == NULL) {
            // Signature is missing altogether.
            bootloaderIncrementBadEventCounter(MISSING_END_OF_INPUT_SIGNATURE);
        } else {
            // pSignature now points to the 1st character after END-OF-INPUT which typically is a "blank".
            //
            // Parse the remaining section of the command for the checksum and number of chars.
            // Apply the following criteria:
            //   - the [0,1] characters must be 0x
            //   - the [2,3,4,5,6,7,8,9] are expected to be digits
            //   - the [10] must be a space
            // beyond the END-OF-INPUT signature: 0x000009CA 25
            //                                    01234567890123
            // Start by evaluating the rank of the presumed "0" character of the checksum in pSignature:
            checksumRank = strlen(END_OF_INPUT_SIGNATURE_STRING) + 1; // checksumRank now indexes the "0" of the checksum string
            if ((char)pSignature[checksumRank]      == '0' &&
                (char)pSignature[checksumRank + 1]  == 'x' &&
                (char)pSignature[checksumRank + 10] == ' ') {
                // The [0]/[1] chars are "0" and "x" and the [10] char is a space, so far so good.
                // Extract the digits that form the actual checksum. Advance the pointer.
                // If the user passed garbage, the checksum will not compute, the command
                // is dropped.
                memset(&pChecksumString[0], 0, 10);                            // clear the checksum-only string
                memcpy(&pChecksumString[0], &pSignature[checksumRank + 2], 8); // copy the hex digits only
                extractedCheckSum  = strtol(pChecksumString, NULL, 16);
                pNumChecksumDigits = &pSignature[checksumRank + 11];
                numChecksumDigits  = atoi(pNumChecksumDigits);

                // Now calculate the checksum from the user's input over the prescribed number of characters.
                calculatedChecksum = 0;
                for (iggy = 0; iggy < numChecksumDigits; iggy++) {
                    calculatedChecksum += pUserInput[iggy];
                }
                if (calculatedChecksum == extractedCheckSum) {
                    checksumValid = TRUE;
                } else {
                    bootloaderIncrementBadEventCounter(INPUT_CHECKSUM_GENERAL_ERROR);
                }
            } else {
                bootloaderIncrementBadEventCounter(INVALID_END_OF_INPUT_FORMAT);
            }
        }
    } else {
        // Input checksum not required. Typically the case when the CLI input interface is a user directly
        // communicating to the AMU over, say a teraterm connection. If the connection to the SBC/Gateway
        // controller is re-established, the interface remains broken until the command is re-entered which
        // must be entered but without the END_OF_INPUT tag - and that's not going to happen. Or: look for
        // the END-OF_INPOUT tag right here and now: if it is detected, then restore this setting to its default.
        pSignature = pApparent_strstr(pUserInput, (char *)&END_OF_INPUT_SIGNATURE_STRING);
        if (pSignature == NULL) { /* nothing to do */                           }
        else                    { /* restore       */ toggleChecksumRequired(); }
        checksumValid = TRUE;
    }
    return checksumValid;
}


// findComponentStringData
//
// A service routine to support CLI. Given a user input argument string, this routine will:
//
// - search for the occurrence of the specified component string
// - if the component string is found, write what follows it to the output string
//
// Example:
//
// - given a user input argument string, say:    sn=456789 model=randy_take_1 hw_version=0.1.7
//
// - make the function call:     findComponentStringData(CLI_ARG_MODEL_STRING, pOutputString, pArguments);
//
// - where CLI_ARG_MODEL_STRING is defined as:    const char CLI_ARG_MODEL_STRING[] = "model";
//
// - then: - the string data for component "model" is found
//         - ensure the = follows the string "model"
//         - find the rest of the string that follows it up to the next blank space or NULL CHAR
//         - the string   randy_take_1   is found and written to pOutputString
//
// The pOutputString parameter that is passed is expected to have been pre-initialized by the user with NULL CHAR.
// If the component is not found, this function simply returns; it is up to the user to check if pOutputString
// string length is zero (component not found) or greater than 0/zero.
//
static void findComponentStringData(const char *pComponent, char *pOutputString, char *pArguments)
{
    U16   argumentLength;
    U16   componentLength;
    U16   iggy;
    U16   pop;
    char *pValueLocateString;
    char  someChar;
    BOOL  endOfValueFound;

    componentLength = strlen(pComponent);
    argumentLength  = strlen(pArguments);

    for (iggy = 0; iggy < argumentLength; iggy++) {

        if (iggy == (argumentLength + 1)) { /* printf("GONE TOO FAR\r\n"); */ break; }  // <- gone too far without finding the component

        if (strncmp(pComponent, &pArguments[iggy], componentLength) == 0) {
            // Found the desired argument. Locate the = character, and locate the string value of this argument.
            //printf("FOUND %s AT RANK %d\r\n", pComponent, iggy);
            pValueLocateString = &pArguments[iggy + componentLength];
            // This char has to be the = character.
            if (pValueLocateString[0] == '=') {
                // Extract the remaining set of characters up to the next empty space or end of string.
                // Point to the next character after the = character, then locate the
                // rank of the next space or end-of-string.
                pValueLocateString++;
                pop = 0;
                endOfValueFound = FALSE;
                while (!endOfValueFound) {
                    someChar = pValueLocateString[pop++];
                    if (someChar == ' ' || someChar == 0) {
                        // Found it.
                        endOfValueFound = TRUE;
                    } else {
                        if (pop >= argumentLength) {
                            // Went too far.
                            pop = 0;
                            endOfValueFound = TRUE;
                        } else {
                            // A character that will be copied. Is the argument itself too long?
                            if (pop >= AMU_COMPONENT_DATA_SIZE_MAX) {
                                // The component is too long.
                                pop = 0;
                                endOfValueFound = TRUE;
                            }
                        }
                    }
                } // while ...
                if (pop > 0) {
                    // Copy over the value string.
                    //printf("COPY %d chars of  %s to dest\r\n", pop-1, pValueLocateString);
                    memcpy(pOutputString, pValueLocateString, pop-1);
                } else {
                    // Nothing of value found, nothing written to the output buffer.
                    //printf("DID NOT FIND END OF VALUE\r\n");
                }
            } else {
                // The = character was expected, not found. Invalid user input format.
                //printf("character %X should have been the = character\r\n", pOutputString[0]);
                break;
            }
            break; // Out of for loop
        } else {
            // Not a string match - keep searching.
        }
    } // for ...
        
}


// findEndOfInputRank
//
// Find the rank into the string where " END-OF-INPUT ..." signature is located. 
//
static U32 findEndOfInputRank(char * pUserInput)
{
    U32  iggy;
    U32  signatureLength;
    U32  endOfInputRank = 0;
    U32  maxLengthToSearch;

    signatureLength   = strlen(END_OF_INPUT_SIGNATURE_STRING);
    maxLengthToSearch = strlen(pUserInput) - signatureLength;

    for (iggy = 0; iggy < maxLengthToSearch; iggy++) {
        if (strncmp(&pUserInput[iggy], END_OF_INPUT_SIGNATURE_STRING, signatureLength) == 0) {
            // Found the rank of the end-of-input signature. The signature is preceded by a white space,
            // it is to be excluded, so the value of 1 is subtracted from the rank.
            endOfInputRank = iggy - 1;
            break;
        } else {
            // Not found - keep searching.
        }
    }
    return endOfInputRank;
}


// displaySbcDebugGameOnNotice
//
// Output "BOOT GAME ON!" to the SBC/DEBUG port.
//
void displaySbcDebugGameOnNotice(void)
{
    char pIttyBittyString[24];
    sprintf(&pIttyBittyString[strlen(pIttyBittyString)], "BOOT GAME ON!\r\n");
    usart_serial_write_packet((usart_if)UART1, (uint8_t *)pIttyBittyString, strlen(pIttyBittyString));
    return;
}




//
//
//  U T I L I T Y      F U N C T I O N S
//
//




// pApparent_strstr
//
//
//
char *pApparent_strstr(char *pSomeString, char *pSearchPart)
{
    char *pLocatedString = NULL;
    U32 iggy;
    U32 searchPartLength = strlen(pSearchPart);

    for (iggy = 0; iggy < SBCCOM_INPUT_BUFFER_LENGTH; iggy++) {
        //
        if (strncmp(&pSomeString[iggy], pSearchPart, searchPartLength) == 0) {
            // Found it.
            pLocatedString = &pSomeString[iggy];
            break;
        }
    }
    return pLocatedString;
}


// Imported from "A Collection of 32-bit CRC Tables and Algorithms" at:
// http://www.mrob.com/pub/comp/crc-all.html
U32 calcCrc32Checksum(U32 crc, U8 *buf, U16 len)
{
    U8 *end;
    crc = ~crc;
    for (end = buf + len; buf < end; ++buf) {
        crc = CRC32_TABLE[(crc ^ *buf) & 0xff] ^ (crc >> 8);
    }
    return ~crc;
}


// Imported from the SG424 universe. But 0 is valid. So is FFFFFFFF.
// So it was converted to a BOOL, with value written to pointer.
BOOL newATOH(const char *String, U32 *pSomeLong)
{
    BOOL returnValue = TRUE;
    U32  value = 0;
    int  digit;
    char c;
    while ((c = *String++) != '\0') {
        if (c >= '0' && c <= '9')
        digit = (unsigned) (c - '0');
        else if (c >= 'a' && c <= 'f')
        digit = (unsigned) (c - 'a') + 10;
        else if (c >= 'A' && c <= 'F')
        digit = (unsigned) (c - 'A') + 10;
        else
        {
            //printf("BAD\r\n");
            value=0;
            returnValue = FALSE;
            break;
        }
        value = (value << 4) + digit;
    } // while ...
    // Now write the value.
    *pSomeLong = value;
    return returnValue;
}


// Imported from the SG424 universe.
U32 atoh(const char *String)
{
    U32 value = 0;
    int digit;
    char oneChar;
    while ((oneChar = *String++) != '\0') {
        if (oneChar >= '0' && oneChar <= '9')
        digit = (unsigned) (oneChar - '0');
        else if (oneChar >= 'a' && oneChar <= 'f')
        digit = (unsigned) (oneChar - 'a') + 10;
        else if (oneChar >= 'A' && oneChar <= 'F')
        digit = (unsigned) (oneChar - 'A') + 10;
        else
        {
            printf("BAD string=%s\r\n", String);
            value = 0;
            break;
        }
        value = (value << 4) + digit;
    }
    return value;
}


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
