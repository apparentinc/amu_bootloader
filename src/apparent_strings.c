/**
 *
 * \brief Apparent strings-only definition module on the AMU (Apparent Management Unit).
 *
 * Copyright (c) 2022 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 * ----- Licensing info, if any, should go here. -----
 *
 *
 * This module contains strings-only definitions, mostly for CLI processing and response to the caller.
 *
 *
 */


const char CLI_STRING[]                            = "\"CLI\"";
const char COMMAND_STRING[]                        = "\"COMMAND\"";
const char ARGUMENTS_STRING[]                      = "\"ARGUMENTS\"";
const char RESULT_STRING[]                         = "\"RESULT\"";
const char SUCCESS_STRING[]                        = "\"SUCCESS\"";
const char FAIL_STRING[]                           = "\"FAIL\"";
const char RESPONSE_STRING[]                       = "\"RESPONSE\"";
const char CODE_STRING[]                           = "\"CODE\"";
const char PROGRAM_STRING[]                        = "\"PROGRAM\"";
const char FLASH_LOAD_ADDRESS_STRING[]             = "\"FLASH LOAD ADDRESS\"";
const char BOOTLOADER_STRING[]                     = "\"BOOTLOADER\"";
const char APPLICATION_STRING[]                    = "\"APPLICATION\"";
const char NONE_STRING[]                           = "\"NONE\"";
const char FLASH_SIGNATURES_STRING[]               = "\"FLASH SIGNATURES\"";
const char AUTONOMOUS_EVENTS_LIST_STRING[]         = "\"AUTO EVENTS LIST\"";
const char INVALID_COMMAND_STRING[]                = "\"invalid command\"";
const char RESET_STRING[]                          = "\"RESET\"";
const char RESET_IN_5_SECONDS_STRING[]             = "\"RESET IN 5 SECONDS\"";

const char RELEASE_STRING[]                        = "\"RELEASE\"";

const char AMU_COUNTERS_STRING[]                   = "\"AMU COUNTERS\"";
const char RANK_STRING[]                           = "\"RANK\"";
const char COUNT_STRING[]                          = "\"COUNT\"";

const char STATE_STRING[]                          = "\"state\"";
const char LAST_TICK_STRING[]                      = "\"lastTick\"";
const char APP_SIG_ERASED_STRING[]                 = "\"APP sig erased\"";
const char FLASH_ADDR_CRC_STRING[]                 = "\"flash addr/crc\"";

const char  AMU_STARTUP_STRING[]                  = "\"AMU STARTUP\"";
const char  UPTIME_STRING[]                       = "\"UPTIME (SECS)\"";

const char  SET_SIGNATURE_RESULT_STRING[]         = "\"SET SIG RESULT\"";
const char  UNLOCK_FAIL_STRING[]                  = "\"UNLOCK FAIL\"";
const char  ERASE_PAGE_FAIL_STRING[]              = "\"ERASE PAGE FAIL\"";
const char  WRITE_LONG_FAIL_STRING[]              = "\"WRITE LONG FAIL\"";
const char  LOCK_FAIL_STRING[]                    = "\"LOCK FAIL\"";
const char  ENABLE_SECURITY_FAIL_STRING[]         = "\"ENABLE SECURITY FAIL\"";
const char  CLI_INPUT_COMMAND_TOO_LONG_STRING[]   = "\"INPUT COMMAND TOO LONG\"";

const char  CLI_COMMAND_EQUAL_NOT_FOUND_STRING[]  = "\"COMMAND EQUAL NOT FOUND\"";
const char  CLI_DATA_FIELD_TOO_LONG_STRING[]      = "\"DATA FIELD TOO LONG\"";
const char  CLI_DATA_EQUAL_NOT_FOUND_STRING[]     = "\"DATA EQUAL NOT FOUND\"";
const char  CLI_INVALID_UPGRADE_COMMAND_STRING[]  = "\"INVALID UPGRADE COMMAND\"";

const char CLI_ARG_ADDR_STRING[]                           = "addr";
const char CLI_ARG_LENGTH_STRING[]                         = "len";
const char CLI_ARG_VALUE_STRING[]                          = "value";
const char CLI_ARG_PORT_STRING[]                           = "port";
const char CLI_ARG_CONFIG_STRING[]                         = "config";
const char CLI_ARG_COMMAND_STRING[]                        = "command";
const char CLI_ARG_LONG_STRING[]                           = "long";

const char CLI_ARG_UPGRADE_START_STRING[]                  = "start";
const char CLI_ARG_UPGRADE_DATA_STRING[]                   = "data";
const char CLI_ARG_UPGRADE_END_STRING[]                    = "end";
const char CLI_ARG_UPGRADE_COMMIT_STRING[]                 = "commit";
const char CLI_ARG_UPGRADE_CANCEL_STRING[]                 = "cancel";
const char CLI_ARG_UPGRADE_STATS_STRING[]                  = "stats";

const char CLI_ARG_COMPONENT_APP_STRING[]                  = "app";
const char CLI_ARG_COMPONENT_BOOT_STRING[]                 = "boot";

const char CLI_ARG_DATA_EQUAL_STRING[]                     = "data=";

const char END_OF_INPUT_SIGNATURE_STRING[]                 = "END-OF-INPUT";
const char END_OF_OUTPUT_SIGNATURE_STRING[]                = "END-OF-OUTPUT";

const char  UPGRADE_STATE_MACHINE_STRING[]                   = "\"UPGRADE STATE MACHINE\"";
const char  UPGRADE_PACKET_SIZE_STRING[]                     = "\"UPGRADE PACKET SIZE\"";

const char SM_INVALID_STATE_STRING[]                         = "\"INVALID\"";
const char SM_IDLE_STATE_STRING[]                            = "\"IDLE\"";
const char SM_WAIT_NEXT_DATA_PACKET_STATE_STRING[]           = "\"WAIT NEXT DATA PACKET\"";
const char SM_WAIT_END_TRANSFER_STATE_STRING[]               = "\"WAIT END TRANSFER\"";
const char SM_WAIT_APP_FINAL_COMMIT_STATE_STRING[]           = "\"WAIT APP FINAL COMMIT\"";


// "Positive" ACKnowledgement strings:
const char  START_ACK_STRING[]                               = "\"start ack\"";
const char  RECEIVE_ACK_STRING[]                             = "\"receive ack\"";
const char  END_ACK_STRING[]                                 = "\"end ack\"";
const char  FINAL_COMMIT_ACK_STRING[]                        =  "\"commit ack\"";
const char  CANCEL_DATA_PKT_STATE_ACK_STRING[]               = "\"cancel in data packet state ack\"";
const char  CANCEL_WAIT_END_TRANSFER_STATE_ACK_STRING[]      = "\"cancel in wait end transfer state ack\"";
const char  CANCEL_WAIT_COMMIT_STATE_ACK_STRING[]            = "\"cancel in wait commit state ack\"";
const char  CANCEL_IDLE_STATE_ACK_STRING[]                   = "\"cancel in idle state ack\"";
const char  STATS_ACK_STRING[]                               = "\"stats ack\"";

// "Negative" N-ACKnowledgements when bad things happen:
const char  AMU_CODING_ERROR_STRING[]                        = "TERRIBLE AMU CODING ERROR"; // <- never expected. Unless the impossible happens.
const char  XFER_COMPONENT_HEX_CONVERT_FAIL_NACK_STRING[]    = "\"XFER COMPONENT HEX CONVERT FAIL NACK\"";
const char  XFER_COMPONENT_CHECKSUM_FAIL_NACK_STRING[]       = "\"XFER COMPONENT CHECKSUM FAIL NACK\"";
const char  XFER_COMPONENT_EVEN_LENGTH_NACK_STRING[]         = "\"XFER COMPONENT EVEN LENGTH NACK\"";
const char  XFER_COMPONENT_MISSING_COLON_NACK_STRING[]       = "\"XFER COMPONENT MISSING COLON NACK\"";
const char  INVAL_STATE_MACHINE_COMMAND_NACK_STRING[]        = "\"INVALID STATE MACHINE COMMAND NACK\"";
const char  INVAL_CMD_IDLE_STATE_NACK_STRING[]               = "\"INVALID UPGRADE COMMAND IN IDLE STATE NACK\"";
const char  INVAL_CMD_NEXT_DATA_PKT_STATE_NACK_STRING[]      = "\"INVALID UPGRADE COMMAND IN NEXT DATA PKT STATE NACK\"";
const char  INVAL_CMD_END_TRANSFER_STATE_NACK_STRING[]       = "\"INVALID UPGRADE COMMAND IN END TRANSFER STATE NACK\"";
const char  XFER_PACKET_COMPONENT_TOO_LONG_NACK_STRING[]     = "\"XFER PACKET COMPONENT TOO LONG NACK\"";
const char  SEGMENT_ADDRESS_INVAL_LENGTH_NACK_STRING[]       = "\"SEGMENT ADDRESS INVAL LENGTH NACK\"";
const char  SEG_ADDR_BUT_HOLD_BUFF_NOT_EMPTY_NACK_STRING[]   = "\"SEG ADDR RECORD BUT HOLD BFFER NOT EMPTY NACK\"";
const char  SEG_ADDR_INVALID_LENGTH_NACK_STRING[]            = "\"SEG ADDR DATA RECORD INVALID LENGTH NACK\"";
const char  DATA_START_INVALID_LENGTH_NACK_STRING[]          = "\"DATA START DATA RECORD INVALID LENGTH NACK\"";
const char  DATA_START_ADDRESS_NOT_ZERO_NACK_STRING[]        = "\"DATA START ADDRESS NOT ZERO NACK\"";
const char  EOF_RECORD_INVALID_STRUCTURE_NACK_STRING[]       = "\"EOF RECORD INVALID STRUCTURE NACK\"";
const char  RELATIVE_ADDR_OUT_OF_SEQUENCE_NACK_STRING[]      = "\"RELATIVE ADDR OUT OF SEQUENCE NACK\"";
const char  DATA_RECORD_NOT_WHOLE_LONGS_NACK_STRING[]        = "\"DATA RECORD NOT WHOLE LONGS NACK\"";
const char  INTERIM_HOLDING_BLOCK_COMMIT_FAIL_NACK_STRING[]  = "\"HRC INTERIM HOLDING BLOCK COMMIT FAIL NACK\"";
const char  LAST_HOLDING_BLOCK_COMMIT_FAIL_NACK_STRING[]     = "\"HRC LAST HOLDING BLOCK COMMIT FAIL NACK\"";
const char  EXPECTED_EXT_SEG_ADDR_RECORD_NACK_STRING[]       = "\"HRC EXPECTED EXT SEG ADDR RECORD NACK\"";
const char  EXPECTED_END_OF_FILE_RECORD_NACK_STRING[]        = "\"HRC EXPECTED END OF FILE RECORD NACK\"";
const char  EXPECTED_DATA_RECORD_NACK_STRING[]               = "\"HRC EXPECTED DATA RECORD NACK\"";
const char  UNEXPECTED_SLA_RECORD_NACK_STRING[]              = "\"HRC UNEXPECTED START LINEAR ADDR RECORD NACK\"";
const char  START_EXT_SEG_ADDR_INVALID_NACK_STRING[]         = "\"START EXTENDED SEGMENT ADDRESS INVALID NACK\"";
const char  FLASH_APP_SIGNATURE_ERASE_FAIL_NACK_STRING[]     = "\"FLASH APP SIGNATURE ERASE FAIL NACK\"";
const char  INVALID_COMMAND_IN_COMMIT_STATE_NACK_STRING[]    = "\"INVALID UPGRADE COMMAND IN COMMIT STATE NACK\"";
const char  COMMIT_FAIL_NACK_STRING[]                        = "\"COMMIT FAIL NACK\"";
const char  RELATIVE_ADDR_NOT_ZERO_NACK_STRING[]             = "\"RELATIVE ADDR NOT ZERO NACK\"";
const char  READ_FLASH_TO_HOLDING_FAIL_NACK_STRING[]         = "\"READ FLASH TO HOLDING FAIL NACK\"";
const char  FLASH_ADDRESS_LOW_ZONE_NACK_STRING[]             = "\"FLASH ADDRESS LOW ZONE NACK\"";
const char  END_ADDRESS_WITHIN_BOOT_ZONE_NACK_STRING[]       = "\"END ADDRESS WITHIN BOOT ZONE NACK\"";
const char  START_ADDRESS_WITHIN_BOOT_ZONE_NACK_STRING[]     = "\"START ADDRESS WITHIN BOOT ZONE NACK\"";
const char  WRITE_HOLD_TO_FLASH_GENERIC_FAIL_NACK_STRING[]   = "\"WRITE HOLD TO FLASH GENERIC FAIL NACK\"";
const char  HEX_CHARACTER_NOT_HEX_NACK_STRING[]              = "\"HEX CHARCATER NOT HEX NACK\"";

const char  SAME70_RESET_REASON_STRING[]                     = "\"SAME70 RESET REASON\"";
const char  RESET_INFO_STRING[]                              = "\"RESET INFO\"";
const char  RESET_REASON_UNKNOWN_STRING[]                    = "\"UNKNOWN\"";
const char  RESET_REASON_GENERAL_STRING[]                    = "\"GENERAL\"";
const char  RESET_REASON_BACKUP_STRING[]                     = "\"BACKUP\"";
const char  RESET_REASON_WATCHDOG_STRING[]                   = "\"WATCHDOG\"";
const char  RESET_REASON_SOFTWARE_STRING[]                   = "\"SOFTWARE\"";
const char  RESET_REASON_NRST_LOW_STRING[]                   = "\"NRST LOW\"";


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
