/**
* \file
*
* \brief Apparent strings-only definition module on the AMU (Apparent Management Unit).
*
 * Copyright (c) 2022 Apparent Inc., All rights reserved
 * http://www.apparent.com
*
* ----- Licensing info, if any, should go here. -----
*
* This header file externalizes all the string definitions for the Apparent application.
*
*/

#ifndef _APPARENT_STRINGS_H_
#define _APPARENT_STRINGS_H_


extern const char CLI_STRING[];
extern const char COMMAND_STRING[];
extern const char ARGUMENTS_STRING[];
extern const char RESULT_STRING[];
extern const char SUCCESS_STRING[];
extern const char FAIL_STRING[];
extern const char RESPONSE_STRING[];
extern const char CODE_STRING[];
extern const char PROGRAM_STRING[];
extern const char FLASH_LOAD_ADDRESS_STRING[];
extern const char BOOTLOADER_STRING[];
extern const char APPLICATION_STRING[];
extern const char NONE_STRING[];
extern const char FLASH_SIGNATURES_STRING[];
extern const char AUTONOMOUS_EVENTS_LIST_STRING[];
extern const char INVALID_COMMAND_STRING[];
extern const char RESET_STRING[];
extern const char RESET_IN_5_SECONDS_STRING[];
extern const char RELEASE_STRING[];
extern const char AMU_COUNTERS_STRING[];
extern const char RANK_STRING[];
extern const char COUNT_STRING[];
extern const char STATE_STRING[];
extern const char LAST_TICK_STRING[];
extern const char APP_SIG_ERASED_STRING[];
extern const char FLASH_ADDR_CRC_STRING[];
extern const char AMU_STARTUP_STRING[];
extern const char UPTIME_STRING[];
extern const char SET_SIGNATURE_RESULT_STRING[];
extern const char UNLOCK_FAIL_STRING[];
extern const char ERASE_PAGE_FAIL_STRING[];
extern const char WRITE_LONG_FAIL_STRING[];
extern const char LOCK_FAIL_STRING[];
extern const char ENABLE_SECURITY_FAIL_STRING[];
extern const char CLI_INPUT_COMMAND_TOO_LONG_STRING[];
extern const char CLI_COMMAND_EQUAL_NOT_FOUND_STRING[];
extern const char CLI_DATA_FIELD_TOO_LONG_STRING[];
extern const char CLI_DATA_EQUAL_NOT_FOUND_STRING[];
extern const char CLI_INVALID_UPGRADE_COMMAND_STRING[];
extern const char CLI_ARG_ADDR_STRING[];
extern const char CLI_ARG_LENGTH_STRING[];
extern const char CLI_ARG_VALUE_STRING[];
extern const char CLI_ARG_PORT_STRING[];
extern const char CLI_ARG_CONFIG_STRING[];
extern const char CLI_ARG_COMMAND_STRING[];
extern const char CLI_ARG_LONG_STRING[];
extern const char CLI_ARG_UPGRADE_START_STRING[];
extern const char CLI_ARG_UPGRADE_DATA_STRING[];
extern const char CLI_ARG_UPGRADE_END_STRING[];
extern const char CLI_ARG_UPGRADE_COMMIT_STRING[];
extern const char CLI_ARG_UPGRADE_CANCEL_STRING[];
extern const char CLI_ARG_UPGRADE_STATS_STRING[];
extern const char CLI_ARG_COMPONENT_APP_STRING[];
extern const char CLI_ARG_COMPONENT_BOOT_STRING[];
extern const char CLI_ARG_DATA_EQUAL_STRING[];
extern const char END_OF_INPUT_SIGNATURE_STRING[];
extern const char END_OF_OUTPUT_SIGNATURE_STRING[];
extern const char UPGRADE_STATE_MACHINE_STRING[];
extern const char UPGRADE_PACKET_SIZE_STRING[];
extern const char SM_INVALID_STATE_STRING[];
extern const char SM_IDLE_STATE_STRING[];
extern const char SM_WAIT_NEXT_DATA_PACKET_STATE_STRING[];
extern const char SM_WAIT_END_TRANSFER_STATE_STRING[];
extern const char SM_WAIT_APP_FINAL_COMMIT_STATE_STRING[];
extern const char START_ACK_STRING[];
extern const char RECEIVE_ACK_STRING[];
extern const char END_ACK_STRING[];
extern const char FINAL_COMMIT_ACK_STRING[];
extern const char CANCEL_DATA_PKT_STATE_ACK_STRING[];
extern const char CANCEL_WAIT_END_TRANSFER_STATE_ACK_STRING[];
extern const char CANCEL_WAIT_COMMIT_STATE_ACK_STRING[];
extern const char CANCEL_IDLE_STATE_ACK_STRING[];
extern const char STATS_ACK_STRING[];
extern const char AMU_CODING_ERROR_STRING[];
extern const char XFER_COMPONENT_HEX_CONVERT_FAIL_NACK_STRING[];
extern const char XFER_COMPONENT_CHECKSUM_FAIL_NACK_STRING[];
extern const char XFER_COMPONENT_EVEN_LENGTH_NACK_STRING[];
extern const char XFER_COMPONENT_MISSING_COLON_NACK_STRING[];
extern const char INVAL_STATE_MACHINE_COMMAND_NACK_STRING[];
extern const char INVAL_CMD_IDLE_STATE_NACK_STRING[];
extern const char INVAL_CMD_NEXT_DATA_PKT_STATE_NACK_STRING[];
extern const char INVAL_CMD_END_TRANSFER_STATE_NACK_STRING[];
extern const char XFER_PACKET_COMPONENT_TOO_LONG_NACK_STRING[];
extern const char SEGMENT_ADDRESS_INVAL_LENGTH_NACK_STRING[];
extern const char SEG_ADDR_BUT_HOLD_BUFF_NOT_EMPTY_NACK_STRING[];
extern const char SEG_ADDR_INVALID_LENGTH_NACK_STRING[];
extern const char DATA_START_INVALID_LENGTH_NACK_STRING[];
extern const char DATA_START_ADDRESS_NOT_ZERO_NACK_STRING[];
extern const char EOF_RECORD_INVALID_STRUCTURE_NACK_STRING[];
extern const char RELATIVE_ADDR_OUT_OF_SEQUENCE_NACK_STRING[];
extern const char DATA_RECORD_NOT_WHOLE_LONGS_NACK_STRING[];
extern const char INTERIM_HOLDING_BLOCK_COMMIT_FAIL_NACK_STRING[];
extern const char LAST_HOLDING_BLOCK_COMMIT_FAIL_NACK_STRING[];
extern const char EXPECTED_EXT_SEG_ADDR_RECORD_NACK_STRING[];
extern const char EXPECTED_END_OF_FILE_RECORD_NACK_STRING[];
extern const char EXPECTED_DATA_RECORD_NACK_STRING[];
extern const char UNEXPECTED_SLA_RECORD_NACK_STRING[];
extern const char START_EXT_SEG_ADDR_INVALID_NACK_STRING[];
extern const char FLASH_APP_SIGNATURE_ERASE_FAIL_NACK_STRING[];
extern const char INVALID_COMMAND_IN_COMMIT_STATE_NACK_STRING[];
extern const char COMMIT_FAIL_NACK_STRING[];
extern const char RELATIVE_ADDR_NOT_ZERO_NACK_STRING[];
extern const char READ_FLASH_TO_HOLDING_FAIL_NACK_STRING[];
extern const char FLASH_ADDRESS_LOW_ZONE_NACK_STRING[];
extern const char END_ADDRESS_WITHIN_BOOT_ZONE_NACK_STRING[];
extern const char START_ADDRESS_WITHIN_BOOT_ZONE_NACK_STRING[];
extern const char WRITE_HOLD_TO_FLASH_GENERIC_FAIL_NACK_STRING[];
extern const char HEX_CHARACTER_NOT_HEX_NACK_STRING[];
extern const char SAME70_RESET_REASON_STRING[];
extern const char RESET_INFO_STRING[];
extern const char RESET_REASON_UNKNOWN_STRING[];
extern const char RESET_REASON_GENERAL_STRING[];
extern const char RESET_REASON_BACKUP_STRING[];
extern const char RESET_REASON_WATCHDOG_STRING[];
extern const char RESET_REASON_SOFTWARE_STRING[];
extern const char RESET_REASON_NRST_LOW_STRING[];

#endif   /* _APPARENT_STRINGS_H_ */
