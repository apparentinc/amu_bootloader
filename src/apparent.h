/**
* \file
*
* \brief Apparent Application Definitions.
*
* Copyright (c) 2021 Apparent, Inc.
*
*
*
* ----- Licensing info, if any, should go here. -----
*
*
*/

#ifndef _APPARENT_H_
#define _APPARENT_H_


// Is console output on system started enabled? Normally not defined
#undef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP


// Exported globals from the apparent.c module:
extern U32  amuOutputChecksum;
extern U16  charsWrittenByInterrupt;
extern U16  totalUartCharsWritten;

#define TRUE  true
#define FALSE false


// ----------------------------------------------------------------------------
//
// Apparent High-level Application COMPILE SWITCHES:

#define INCLUDE_CLI_FLASH_COMMANDS               FALSE

// This allow the user signature to be erased and re-flashed.
#define USER_SIGNATURE_REFLASH                   TRUE

// The original action of pushbutton 1 (associated to contact closure #1) now removed.
#define PUSH_BUTTON_1_ACTION_ENABLED             FALSE

// The value of the watchdog control restart register:
#define WATCHDOG_CONTROL_RESTART       (U32)( WDT_CR_KEY_PASSWD | WDT_CR_WDRSTT )

// FLASH: the smallest block of flash that can be erased is: 16 pages. Attempting to erase anything
//        smaller simply fails on SAME70. On the other hand, setting the smallest block that can be
//        erased to 32 pages means that the signature zone will need to be significantly larger just
//        to hold a 32-bit long. So 16 pages is the best compromise. 
//
// This provides a "flash holding block" size of: 512*16 = 8192 = 0x2000 bytes
// An upgrade holding block in RAM is declared with this size. This seems like
// a lot of RAM, however: plenty exists on the SAME70, and next to nothing else
// is going on in the bootloader anyway. 
#define APPARENT_IFLASH_ERASE_PAGES              IFLASH_ERASE_PAGES_16
#define MAX_FLASH_ERASE_BLOCK_SIZE               IFLASH_PAGE_SIZE*16
#define MAX_FLASH_HOLDING_BLOCK_BYTES            MAX_FLASH_ERASE_BLOCK_SIZE
#define MAX_FLASH_HOLDING_BLOCK_LONGS            (MAX_FLASH_HOLDING_BLOCK_BYTES / 4)


// END OF SWITCHES ...
//
// ----------------------------------------------------------------------------


// NOTE: the following "HIGH-LEVEL SAME70 INTERNAL FLASH INFO" section would normally
//       be in the flashninator.h header file. However, 2 other projects (BOOTSTRAP and
//       APPLICATION) have the identical information in their respective apparent.h header
//       files 

// ----------------------------------------------------------------------------
//
// HIGH-LEVEL SAME70 INTERNAL FLASH INFO
//
// This small section is expected to be identical on the following 3 git repos:
//
// - AMU_BOOTSTRAP
// - AMU_BOOTLOADER
// - AMU
//
// Program signature values. They are used to determine if a program (app or boot)
// is valid in order to jump to it.
//
// It is good practice to use prime numbers in a situation like this. Prime numbers,
// 32-bit especially, can be found on the net. At random, the following were chosen:
//
// -> 3769807637 = 0xE0B2B315
// ->  940609807 = 0x3810910F
#define APPLICATION_SIGNATURE_VALUE         0xE0B2B315UL
#define BOOTLOADER_SIGNATURE_VALUE          0x3810910FUL
#define NULL_SIGNATURE_VALUE                0xFFFFFFFFUL


// Program signature addresses. They are located in sector[1] which starts at 0x20000.
// They are offset by 0x2000 bytes in flash, since that is the smallest chunk of flash
// that can be erased. Each will hold a single 32-bit "signature" as defined above. It
// seems like a waste that 0x2000 bytes (8k!!!) of memory will be used to store a single
// 32-bit number. When completing the final actions of a program upgrade, the final commit
// must be the smallest atomic action the CPU can perform, in this case: write a 32-bit
// number to flash.
#define APPLICATION_SIGNATURE_ADDRESS       0x00020000UL
#define BOOTLOADER_SIGNATURE_ADDRESS        0x00022000UL

// Define the limit of the flash address where the bootstrap zone/app-boot signature reside.
#define SIGNATURE_INFO_LIMIT_ADDRESS        BOOTLOADER_SIGNATURE_ADDRESS + MAX_FLASH_ERASE_BLOCK_SIZE


// Program load/jump addresses. They are located to start on a sector boundary.
// There are 16 sectors of flash, with each sector holding 0x20000 bytes of memory.
//
// - sector [0]      at 0x0      contains the bootstrap,
// - sector [1]      at 0x20000  contains the program signatures
// - sector [2..11]  at 0x40000  contains the primary AMU Apparent application = 10 sectors worth of flash = 0x140000bytes = 1.3Mbytes
// - sectors[12..15] at 0x180000 contains the bootloader                       =  4 sectors worth of flash = 0x080000bytes = 524 288 bytes
#define APP_PROGRAM_SECTORS_MAX            10
#define BOOT_PROGRAM_SECTORS_MAX            4

#define APPARENT_APPLICATION_ADDRESS        0x00040000UL
#define BOOTLOADER_FLASH_ADDRESS            0x00180000UL
//
//
// END OF HIGH-LEVEL SAME70 INTERNAL FLASH INFO
//
// ----------------------------------------------------------------------------


// How often should the relay/LED flash when enabled:
#define RELAY_FLASH_DELAY                    10000

#define APPARENT_BOARD_MODEL           "31-0024 PCBA"
#define APPARENT_BOARD_HW_VERSION      "0.1"


#define APPARENT_SYSTEM_BOOTLOADER_HEADER \
"\r\n\r\n---------------------------------------------------\r\n" \
"--\r\n-- Apparent Gateway Peripheral PBCA BOOTLOADER\r\n" \
"-- Copyright 2021 Apparent, Inc.\r\n" \
"-- Compiled: "__DATE__" "__TIME__"\r\n"

//"-- Model: 31-0024 PCBA\r\n"
//"-- Product:\r\n"
//"-- Serial:\r\n"
//"-- HW Ver.Rev      0.0\r\n"


// Until the group decides what the release/version should look like, it's just a number
// based on the COMMIT NOTES.
#define APPARENT_RELEASE_TITLE_STRING  "-- Apparent Inc. bootloader release R"
#define APPARENT_RELEASE_STRING        "17"


// To support the bad events counters.
typedef enum {
    UART0_INPUT_BUFFER_OVERFLOW              =  0,         // <-----------
    UART1_INPUT_BUFFER_OVERFLOW              =  1,         //            |
    UART0_INPUT_BUFFER_ABOVE_SAFETY_MARGIN   =  2,         //            |
    UART1_INPUT_BUFFER_ABOVE_SAFETY_MARGIN   =  3,         //            |
    INPUT_CHECKSUM_GENERAL_ERROR             =  4,         //     common on bootloader and application
    MISSING_END_OF_INPUT_SIGNATURE           =  5,         //            |
    INVALID_END_OF_INPUT_FORMAT              =  6,         //            |
    SBC_INPUT_WHILE_PROCESSING               =  7,         //            |
    INVAL_COUNTER_TO_INCREMENT               =  8,         // <-----------
    
    // Other definitions are made, but on the application project, not the bootloader.
    MAX_BAD_EVENTS_COUNTERS                  =  9          // <- always last, reflecting list size.
} e_bad_events_counters;





/*----------------------------------------------------------------------------*/

// 2 plain serial ports over micro USB: - SBCCOM port interface over J21
//                                      - CRAFT port interface over J50
//
// By default: standard I/O IS OVER THE SBC COM/J21 INTERFACE.
#define CONSOLE_UART         UART0
#define CONSOLE_UART_ID      ID_UART0
#define STDIO_UART           (usart_if)UART0


// Input buffer for the SCB COM and Craft ports. Might need to be increased. Depending on requirements,
// either or both may need to be increased. The size of the SBC COM port is set to the same as for the
// BOOTLOADER program, which needs at least 512 bytes + a few extra overhead bytes for the firmware
// upgrade feature.
#define SBCCOM_INPUT_BUFFER_LENGTH              2048    // increased from 600
#define SBCCOM_INPUT_BUFFER_SAFETY_MARGIN       1946    // about 95% of the buffer
#define CRAFT_INPUT_BUFFER_LENGTH                256    // increased from 128
#define CRAFT_INPUT_BUFFER_SAFETY_MARGIN         244    // about 95% of the buffer
#define UPGRADE_PACKET_DATA_SIZE_MAX            1848    // increased from 512

// The max length of a CLI input argument component. There is no practical limit on the
// number of input arguments other than the above max buffer length.
#define AMU_COMPONENT_DATA_SIZE_MAX    32


#define CRC_32BIT_SEED       0x00000000

// To support upgrade testing in a debug environment:
#define PLEASE_DROP_THE_NEXT_DATA_PACKET         1
#define PLEASE_DROP_THE_NEXT_PACKET_ACK          2

// The SAME70 reset controller info block.
typedef struct s_same70ResetInfoDataBlock
{
    U32 reason;              //
    U32 status;              //
    U32 statusMasked;        //
} t_same70ResetInfoDataBlock;


// SAME70 reset reasons, based on "ASF/sam/utils/cmsis/same70/include/component/rstc.h" header file. Once the reset reason from
// the SAME70's RST register is evaluated, it is mapped to one of the following with a string to be reported to the SBC/Gateway.
#define RESET_REASON_UNKNOWN           1 // in case the reset reason fails to map to the following set
#define RESET_REASON_GENERAL           2 // RSTC_SR_RSTTYP_GENERAL_RST (0x0u << 8) /**< \brief (RSTC_SR) First power-up reset */
#define RESET_REASON_BACKUP            3 // RSTC_SR_RSTTYP_BACKUP_RST  (0x1u << 8) /**< \brief (RSTC_SR) Return from Backup Mode */
#define RESET_REASON_WATCHDOG          4 // RSTC_SR_RSTTYP_WDT_RST     (0x2u << 8) /**< \brief (RSTC_SR) Watchdog fault occurred */
#define RESET_REASON_SOFTWARE          5 // RSTC_SR_RSTTYP_SOFT_RST    (0x3u << 8) /**< \brief (RSTC_SR) Processor reset required by the software */
#define RESET_REASON_NRST_LOW          6 // RSTC_SR_RSTTYP_USER_RST    (0x4u << 8) /**< \brief (RSTC_SR) NRST pin detected low */

#define JSON_OUTPUT                    TRUE
#define NO_JSON_OUTPUT                 FALSE
\

// External Function Prototypes
void  getProcessorResetReason               (void);
void  initApparentSubsystemData             (void);
void  writeToSbcComInputBuffer              (char oneChar);
void  writeToCraftInputBuffer               (char oneChar);
void  incrementAmuCounter                   (U16  counter);
void  initCraftInputBuffer                  (void);
void  initSbcComInputBuffer                 (void);
void  checkSbcComUserInput                  (void);
void  checkCraftUserInput                   (void);
void  checkSoftwareReset                    (void);
void  displayApparentBootLoaderBanner       (void);
void  displayEndOfOutput                    (void);
char *pGimmePrimaryPortString               (U16 primaryPortIndex);
char *pGimmeSecondaryPortString             (U16 secondaryPortIndex);
void  configureOutputContactClosures        (void);
void  configureTempAndFanControl            (void);
void  configurePoeController                (void);
void  checkInputContactClosures             (void);
void  checkTemperatureForFanSpeed           (void);
void  poeUpdateTemperatureRegister          (void);
void  checkPoeController                    (void);
void  displaySbcDebugGameOnNotice           (void);
void  pokeWatchdog                          (void);


// A SMALL SET OF UTILITY FUNCTIONS. EVENTUALLY: MOVE THEM TO A NEW apparent_helper.c/h modules,
//                                               to be contained in the common git submodule.
char  *pApparent_strstr                     (char *pSomeString, char *pSearchPart);
U32   calcCrc32Checksum                     (U32 crcSeed, U8 *buf, U16 len);
BOOL  newATOH                               (const char *String, U32 *pSomeLong);
U32   atoh                                  (const char *String);

/*----------------------------------------------------------------------------*/
#endif   /* _APPARENT_H_ */
